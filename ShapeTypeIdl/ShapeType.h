

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from ShapeType.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef ShapeType_434675311_h
#define ShapeType_434675311_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

static const DDS_Char * QoS_Library_Name= "ShapeType_Library";
static const DDS_Char * Base_Profile_Name= "Base_Profile";
static const DDS_Char * Shapes_Profile_Name= "ShapeType_Profile";
static const DDS_Char * Command_Profile_Name= "Command_Profile";
static const DDS_Char * Square_Topic_Name= "Square";
static const DDS_Char * Triangle_Topic_Name= "Triangle";
static const DDS_Char * Circle_Topic_Name= "Circle";
static const DDS_Long MAX_SHAPE_SIZE= 50;
static const DDS_Long MAX_COORDINATE= 250;
static const DDS_Char * PURPLE_COLOR= "PURPLE";
static const DDS_Char * BLUE_COLOR= "BLUE";
static const DDS_Char * RED_COLOR= "RED";
static const DDS_Char * GREEN_COLOR= "GREEN";
static const DDS_Char * YELLOW_COLOR= "YELLOW";
static const DDS_Char * CYAN_COLOR= "CYAN";
static const DDS_Char * MAGENTA_COLOR= "MAGENTA";
static const DDS_Char * ORANGE_COLOR= "ORANGE";
static const DDS_Long MAX_ANGLE= 360;
typedef enum ShapeFillKind
{
    SOLID_FILL ,      
    TRANSPARENT_FILL ,      
    HORIZONTAL_HATCH_FILL ,      
    VERTICAL_HATCH_FILL      
} ShapeFillKind;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* ShapeFillKind_get_typecode(void); /* Type code */

DDS_SEQUENCE(ShapeFillKindSeq, ShapeFillKind);                                        

NDDSUSERDllExport
RTIBool ShapeFillKind_initialize(
    ShapeFillKind* self);

NDDSUSERDllExport
RTIBool ShapeFillKind_initialize_ex(
    ShapeFillKind* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool ShapeFillKind_initialize_w_params(
    ShapeFillKind* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void ShapeFillKind_finalize(
    ShapeFillKind* self);

NDDSUSERDllExport
void ShapeFillKind_finalize_ex(
    ShapeFillKind* self,RTIBool deletePointers);

NDDSUSERDllExport
void ShapeFillKind_finalize_w_params(
    ShapeFillKind* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void ShapeFillKind_finalize_optional_members(
    ShapeFillKind* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool ShapeFillKind_copy(
    ShapeFillKind* dst,
    const ShapeFillKind* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

extern const char *ShapeTypeTYPENAME;

struct ShapeTypeSeq;
#ifndef NDDS_STANDALONE_TYPE
class ShapeTypeTypeSupport;
class ShapeTypeDataWriter;
class ShapeTypeDataReader;
#endif

class ShapeType 
{
  public:
    typedef struct ShapeTypeSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef ShapeTypeTypeSupport TypeSupport;
    typedef ShapeTypeDataWriter DataWriter;
    typedef ShapeTypeDataReader DataReader;
    #endif

    DDS_Char *   color ;
    DDS_Long   x ;
    DDS_Long   y ;
    DDS_Long   shapesize ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* ShapeType_get_typecode(void); /* Type code */

DDS_SEQUENCE(ShapeTypeSeq, ShapeType);                                        

NDDSUSERDllExport
RTIBool ShapeType_initialize(
    ShapeType* self);

NDDSUSERDllExport
RTIBool ShapeType_initialize_ex(
    ShapeType* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool ShapeType_initialize_w_params(
    ShapeType* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void ShapeType_finalize(
    ShapeType* self);

NDDSUSERDllExport
void ShapeType_finalize_ex(
    ShapeType* self,RTIBool deletePointers);

NDDSUSERDllExport
void ShapeType_finalize_w_params(
    ShapeType* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void ShapeType_finalize_optional_members(
    ShapeType* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool ShapeType_copy(
    ShapeType* dst,
    const ShapeType* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

extern const char *ShapeTypeExtendedTYPENAME;

struct ShapeTypeExtendedSeq;
#ifndef NDDS_STANDALONE_TYPE
class ShapeTypeExtendedTypeSupport;
class ShapeTypeExtendedDataWriter;
class ShapeTypeExtendedDataReader;
#endif

class ShapeTypeExtended 
: public ShapeType{
  public:
    typedef struct ShapeTypeExtendedSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef ShapeTypeExtendedTypeSupport TypeSupport;
    typedef ShapeTypeExtendedDataWriter DataWriter;
    typedef ShapeTypeExtendedDataReader DataReader;
    #endif

    ShapeFillKind   fillKind ;
    DDS_Float   angle ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* ShapeTypeExtended_get_typecode(void); /* Type code */

DDS_SEQUENCE(ShapeTypeExtendedSeq, ShapeTypeExtended);                                        

NDDSUSERDllExport
RTIBool ShapeTypeExtended_initialize(
    ShapeTypeExtended* self);

NDDSUSERDllExport
RTIBool ShapeTypeExtended_initialize_ex(
    ShapeTypeExtended* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool ShapeTypeExtended_initialize_w_params(
    ShapeTypeExtended* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void ShapeTypeExtended_finalize(
    ShapeTypeExtended* self);

NDDSUSERDllExport
void ShapeTypeExtended_finalize_ex(
    ShapeTypeExtended* self,RTIBool deletePointers);

NDDSUSERDllExport
void ShapeTypeExtended_finalize_w_params(
    ShapeTypeExtended* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void ShapeTypeExtended_finalize_optional_members(
    ShapeTypeExtended* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool ShapeTypeExtended_copy(
    ShapeTypeExtended* dst,
    const ShapeTypeExtended* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif
static const DDS_Char * ShapeCC_Topic_Name= "ShapeCC";
static const DDS_Char * ShapeCCResponse_Topic_Name= "ShapeCC_Response";
namespace ShapeServ {
    typedef    DDS_Long   size_t ;
    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, start exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport __declspec(dllexport)
    #endif

    NDDSUSERDllExport DDS_TypeCode* size_t_get_typecode(void); /* Type code */

    DDS_SEQUENCE(size_tSeq, size_t);                                        

    NDDSUSERDllExport
    RTIBool size_t_initialize(
        size_t* self);

    NDDSUSERDllExport
    RTIBool size_t_initialize_ex(
        size_t* self,RTIBool allocatePointers,RTIBool allocateMemory);

    NDDSUSERDllExport
    RTIBool size_t_initialize_w_params(
        size_t* self,
        const struct DDS_TypeAllocationParams_t * allocParams);        

    NDDSUSERDllExport
    void size_t_finalize(
        size_t* self);

    NDDSUSERDllExport
    void size_t_finalize_ex(
        size_t* self,RTIBool deletePointers);

    NDDSUSERDllExport
    void size_t_finalize_w_params(
        size_t* self,
        const struct DDS_TypeDeallocationParams_t * deallocParams);

    NDDSUSERDllExport
    void size_t_finalize_optional_members(
        size_t* self, RTIBool deletePointers);  

    NDDSUSERDllExport
    RTIBool size_t_copy(
        size_t* dst,
        const size_t* src);

    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, stop exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport
    #endif
    typedef enum cmd_t
    {
        FILL ,      
        ANGLE      
    } cmd_t;
    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, start exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport __declspec(dllexport)
    #endif

    NDDSUSERDllExport DDS_TypeCode* cmd_t_get_typecode(void); /* Type code */

    DDS_SEQUENCE(cmd_tSeq, cmd_t);                                        

    NDDSUSERDllExport
    RTIBool cmd_t_initialize(
        cmd_t* self);

    NDDSUSERDllExport
    RTIBool cmd_t_initialize_ex(
        cmd_t* self,RTIBool allocatePointers,RTIBool allocateMemory);

    NDDSUSERDllExport
    RTIBool cmd_t_initialize_w_params(
        cmd_t* self,
        const struct DDS_TypeAllocationParams_t * allocParams);        

    NDDSUSERDllExport
    void cmd_t_finalize(
        cmd_t* self);

    NDDSUSERDllExport
    void cmd_t_finalize_ex(
        cmd_t* self,RTIBool deletePointers);

    NDDSUSERDllExport
    void cmd_t_finalize_w_params(
        cmd_t* self,
        const struct DDS_TypeDeallocationParams_t * deallocParams);

    NDDSUSERDllExport
    void cmd_t_finalize_optional_members(
        cmd_t* self, RTIBool deletePointers);  

    NDDSUSERDllExport
    RTIBool cmd_t_copy(
        cmd_t* dst,
        const cmd_t* src);

    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, stop exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport
    #endif

    extern const char *cmd_typeTYPENAME;

    struct cmd_typeSeq;
    #ifndef NDDS_STANDALONE_TYPE
    class cmd_typeTypeSupport;
    class cmd_typeDataWriter;
    class cmd_typeDataReader;
    #endif

    typedef struct cmd_type {
        typedef struct cmd_typeSeq Seq;
        #ifndef NDDS_STANDALONE_TYPE
        typedef cmd_typeTypeSupport TypeSupport;
        typedef cmd_typeDataWriter DataWriter;
        typedef cmd_typeDataReader DataReader;
        #endif

        ShapeServ::cmd_t _d;
        struct cmd_type_u 
        {

            ShapeFillKind   fill ;
            DDS_Float   angle ;
        }_u;

    } cmd_type ;
    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, start exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport __declspec(dllexport)
    #endif

    NDDSUSERDllExport DDS_TypeCode* cmd_type_get_typecode(void); /* Type code */

    DDS_SEQUENCE(cmd_typeSeq, cmd_type);                                        

    NDDSUSERDllExport
    RTIBool cmd_type_initialize(
        cmd_type* self);

    NDDSUSERDllExport
    RTIBool cmd_type_initialize_ex(
        cmd_type* self,RTIBool allocatePointers,RTIBool allocateMemory);

    NDDSUSERDllExport
    RTIBool cmd_type_initialize_w_params(
        cmd_type* self,
        const struct DDS_TypeAllocationParams_t * allocParams);        

    NDDSUSERDllExport
    void cmd_type_finalize(
        cmd_type* self);

    NDDSUSERDllExport
    void cmd_type_finalize_ex(
        cmd_type* self,RTIBool deletePointers);

    NDDSUSERDllExport
    void cmd_type_finalize_w_params(
        cmd_type* self,
        const struct DDS_TypeDeallocationParams_t * deallocParams);

    NDDSUSERDllExport
    void cmd_type_finalize_optional_members(
        cmd_type* self, RTIBool deletePointers);  

    NDDSUSERDllExport
    RTIBool cmd_type_copy(
        cmd_type* dst,
        const cmd_type* src);

    NDDSUSERDllExport
    DDS_LongLong cmd_type_getDefaultDiscriminator();

    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, stop exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport
    #endif

    extern const char *shape_change_cmdTYPENAME;

    struct shape_change_cmdSeq;
    #ifndef NDDS_STANDALONE_TYPE
    class shape_change_cmdTypeSupport;
    class shape_change_cmdDataWriter;
    class shape_change_cmdDataReader;
    #endif

    class shape_change_cmd 
    {
      public:
        typedef struct shape_change_cmdSeq Seq;
        #ifndef NDDS_STANDALONE_TYPE
        typedef shape_change_cmdTypeSupport TypeSupport;
        typedef shape_change_cmdDataWriter DataWriter;
        typedef shape_change_cmdDataReader DataReader;
        #endif

        DDS_Long   sensor_id ;
        ShapeServ::size_t   size ;
        ShapeServ::cmd_type   command ;

    };
    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, start exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport __declspec(dllexport)
    #endif

    NDDSUSERDllExport DDS_TypeCode* shape_change_cmd_get_typecode(void); /* Type code */

    DDS_SEQUENCE(shape_change_cmdSeq, shape_change_cmd);                                        

    NDDSUSERDllExport
    RTIBool shape_change_cmd_initialize(
        shape_change_cmd* self);

    NDDSUSERDllExport
    RTIBool shape_change_cmd_initialize_ex(
        shape_change_cmd* self,RTIBool allocatePointers,RTIBool allocateMemory);

    NDDSUSERDllExport
    RTIBool shape_change_cmd_initialize_w_params(
        shape_change_cmd* self,
        const struct DDS_TypeAllocationParams_t * allocParams);        

    NDDSUSERDllExport
    void shape_change_cmd_finalize(
        shape_change_cmd* self);

    NDDSUSERDllExport
    void shape_change_cmd_finalize_ex(
        shape_change_cmd* self,RTIBool deletePointers);

    NDDSUSERDllExport
    void shape_change_cmd_finalize_w_params(
        shape_change_cmd* self,
        const struct DDS_TypeDeallocationParams_t * deallocParams);

    NDDSUSERDllExport
    void shape_change_cmd_finalize_optional_members(
        shape_change_cmd* self, RTIBool deletePointers);  

    NDDSUSERDllExport
    RTIBool shape_change_cmd_copy(
        shape_change_cmd* dst,
        const shape_change_cmd* src);

    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, stop exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport
    #endif

    extern const char *shape_change_cmd_wRequestorTYPENAME;

    struct shape_change_cmd_wRequestorSeq;
    #ifndef NDDS_STANDALONE_TYPE
    class shape_change_cmd_wRequestorTypeSupport;
    class shape_change_cmd_wRequestorDataWriter;
    class shape_change_cmd_wRequestorDataReader;
    #endif

    class shape_change_cmd_wRequestor 
    : public ShapeServ::shape_change_cmd{
      public:
        typedef struct shape_change_cmd_wRequestorSeq Seq;
        #ifndef NDDS_STANDALONE_TYPE
        typedef shape_change_cmd_wRequestorTypeSupport TypeSupport;
        typedef shape_change_cmd_wRequestorDataWriter DataWriter;
        typedef shape_change_cmd_wRequestorDataReader DataReader;
        #endif

        DDS_Long   requestor_id ;

    };
    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, start exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport __declspec(dllexport)
    #endif

    NDDSUSERDllExport DDS_TypeCode* shape_change_cmd_wRequestor_get_typecode(void); /* Type code */

    DDS_SEQUENCE(shape_change_cmd_wRequestorSeq, shape_change_cmd_wRequestor);                                        

    NDDSUSERDllExport
    RTIBool shape_change_cmd_wRequestor_initialize(
        shape_change_cmd_wRequestor* self);

    NDDSUSERDllExport
    RTIBool shape_change_cmd_wRequestor_initialize_ex(
        shape_change_cmd_wRequestor* self,RTIBool allocatePointers,RTIBool allocateMemory);

    NDDSUSERDllExport
    RTIBool shape_change_cmd_wRequestor_initialize_w_params(
        shape_change_cmd_wRequestor* self,
        const struct DDS_TypeAllocationParams_t * allocParams);        

    NDDSUSERDllExport
    void shape_change_cmd_wRequestor_finalize(
        shape_change_cmd_wRequestor* self);

    NDDSUSERDllExport
    void shape_change_cmd_wRequestor_finalize_ex(
        shape_change_cmd_wRequestor* self,RTIBool deletePointers);

    NDDSUSERDllExport
    void shape_change_cmd_wRequestor_finalize_w_params(
        shape_change_cmd_wRequestor* self,
        const struct DDS_TypeDeallocationParams_t * deallocParams);

    NDDSUSERDllExport
    void shape_change_cmd_wRequestor_finalize_optional_members(
        shape_change_cmd_wRequestor* self, RTIBool deletePointers);  

    NDDSUSERDllExport
    RTIBool shape_change_cmd_wRequestor_copy(
        shape_change_cmd_wRequestor* dst,
        const shape_change_cmd_wRequestor* src);

    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, stop exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport
    #endif

    extern const char *shape_change_cmd_responseTYPENAME;

    struct shape_change_cmd_responseSeq;
    #ifndef NDDS_STANDALONE_TYPE
    class shape_change_cmd_responseTypeSupport;
    class shape_change_cmd_responseDataWriter;
    class shape_change_cmd_responseDataReader;
    #endif

    class shape_change_cmd_response 
    {
      public:
        typedef struct shape_change_cmd_responseSeq Seq;
        #ifndef NDDS_STANDALONE_TYPE
        typedef shape_change_cmd_responseTypeSupport TypeSupport;
        typedef shape_change_cmd_responseDataWriter DataWriter;
        typedef shape_change_cmd_responseDataReader DataReader;
        #endif

        DDS_Long   requestor_id ;
        DDS_Long   sensor_id ;
        ShapeServ::cmd_type   commanded_command ;
        ShapeServ::size_t   commanded_size ;
        DDS_Boolean   isValid ;

    };
    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, start exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport __declspec(dllexport)
    #endif

    NDDSUSERDllExport DDS_TypeCode* shape_change_cmd_response_get_typecode(void); /* Type code */

    DDS_SEQUENCE(shape_change_cmd_responseSeq, shape_change_cmd_response);                                        

    NDDSUSERDllExport
    RTIBool shape_change_cmd_response_initialize(
        shape_change_cmd_response* self);

    NDDSUSERDllExport
    RTIBool shape_change_cmd_response_initialize_ex(
        shape_change_cmd_response* self,RTIBool allocatePointers,RTIBool allocateMemory);

    NDDSUSERDllExport
    RTIBool shape_change_cmd_response_initialize_w_params(
        shape_change_cmd_response* self,
        const struct DDS_TypeAllocationParams_t * allocParams);        

    NDDSUSERDllExport
    void shape_change_cmd_response_finalize(
        shape_change_cmd_response* self);

    NDDSUSERDllExport
    void shape_change_cmd_response_finalize_ex(
        shape_change_cmd_response* self,RTIBool deletePointers);

    NDDSUSERDllExport
    void shape_change_cmd_response_finalize_w_params(
        shape_change_cmd_response* self,
        const struct DDS_TypeDeallocationParams_t * deallocParams);

    NDDSUSERDllExport
    void shape_change_cmd_response_finalize_optional_members(
        shape_change_cmd_response* self, RTIBool deletePointers);  

    NDDSUSERDllExport
    RTIBool shape_change_cmd_response_copy(
        shape_change_cmd_response* dst,
        const shape_change_cmd_response* src);

    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, stop exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport
    #endif
} /* namespace ShapeServ  */

#endif /* ShapeType */



/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from ShapeType.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include "ShapeTypeSupport.h"
#include "ShapeTypePlugin.h"

#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'ShapeType' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   ShapeTypeTYPENAME

/* Defines */
#define TDataWriter ShapeTypeDataWriter
#define TData       ShapeType

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   ShapeTypeTYPENAME

/* Defines */
#define TDataReader ShapeTypeDataReader
#define TDataSeq    ShapeTypeSeq
#define TData       ShapeType

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    ShapeTypeTYPENAME
#define TPlugin_new  ShapeTypePlugin_new
#define TPlugin_delete  ShapeTypePlugin_delete

/* Defines */
#define TTypeSupport ShapeTypeTypeSupport
#define TData        ShapeType
#define TDataReader  ShapeTypeDataReader
#define TDataWriter  ShapeTypeDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'ShapeTypeExtended' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   ShapeTypeExtendedTYPENAME

/* Defines */
#define TDataWriter ShapeTypeExtendedDataWriter
#define TData       ShapeTypeExtended

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   ShapeTypeExtendedTYPENAME

/* Defines */
#define TDataReader ShapeTypeExtendedDataReader
#define TDataSeq    ShapeTypeExtendedSeq
#define TData       ShapeTypeExtended

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    ShapeTypeExtendedTYPENAME
#define TPlugin_new  ShapeTypeExtendedPlugin_new
#define TPlugin_delete  ShapeTypeExtendedPlugin_delete

/* Defines */
#define TTypeSupport ShapeTypeExtendedTypeSupport
#define TData        ShapeTypeExtended
#define TDataReader  ShapeTypeExtendedDataReader
#define TDataWriter  ShapeTypeExtendedDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

namespace ShapeServ {

    /* ========================================================================= */
    /**
    <<IMPLEMENTATION>>

    Defines:   TData,
    TDataWriter,
    TDataReader,
    TTypeSupport

    Configure and implement 'cmd_type' support classes.

    Note: Only the #defined classes get defined
    */

    /* ----------------------------------------------------------------- */
    /* DDSDataWriter
    */

    /**
    <<IMPLEMENTATION >>

    Defines:   TDataWriter, TData
    */

    /* Requires */
    #define TTYPENAME   cmd_typeTYPENAME

    /* Defines */
    #define TDataWriter cmd_typeDataWriter
    #define TData       ShapeServ::cmd_type

    #include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

    #undef TDataWriter
    #undef TData

    #undef TTYPENAME

    /* ----------------------------------------------------------------- */
    /* DDSDataReader
    */

    /**
    <<IMPLEMENTATION >>

    Defines:   TDataReader, TDataSeq, TData
    */

    /* Requires */
    #define TTYPENAME   cmd_typeTYPENAME

    /* Defines */
    #define TDataReader cmd_typeDataReader
    #define TDataSeq    cmd_typeSeq
    #define TData       ShapeServ::cmd_type

    #include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

    #undef TDataReader
    #undef TDataSeq
    #undef TData

    #undef TTYPENAME

    /* ----------------------------------------------------------------- */
    /* TypeSupport

    <<IMPLEMENTATION >>

    Requires:  TTYPENAME,
    TPlugin_new
    TPlugin_delete
    Defines:   TTypeSupport, TData, TDataReader, TDataWriter
    */

    /* Requires */
    #define TTYPENAME    cmd_typeTYPENAME
    #define TPlugin_new  ShapeServ::cmd_typePlugin_new
    #define TPlugin_delete  ShapeServ::cmd_typePlugin_delete

    /* Defines */
    #define TTypeSupport cmd_typeTypeSupport
    #define TData        ShapeServ::cmd_type
    #define TDataReader  cmd_typeDataReader
    #define TDataWriter  cmd_typeDataWriter
    #define TGENERATE_SER_CODE
    #define TGENERATE_TYPECODE

    #include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

    #undef TTypeSupport
    #undef TData
    #undef TDataReader
    #undef TDataWriter
    #undef TGENERATE_TYPECODE
    #undef TGENERATE_SER_CODE
    #undef TTYPENAME
    #undef TPlugin_new
    #undef TPlugin_delete

    /* ========================================================================= */
    /**
    <<IMPLEMENTATION>>

    Defines:   TData,
    TDataWriter,
    TDataReader,
    TTypeSupport

    Configure and implement 'shape_change_cmd' support classes.

    Note: Only the #defined classes get defined
    */

    /* ----------------------------------------------------------------- */
    /* DDSDataWriter
    */

    /**
    <<IMPLEMENTATION >>

    Defines:   TDataWriter, TData
    */

    /* Requires */
    #define TTYPENAME   shape_change_cmdTYPENAME

    /* Defines */
    #define TDataWriter shape_change_cmdDataWriter
    #define TData       ShapeServ::shape_change_cmd

    #include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

    #undef TDataWriter
    #undef TData

    #undef TTYPENAME

    /* ----------------------------------------------------------------- */
    /* DDSDataReader
    */

    /**
    <<IMPLEMENTATION >>

    Defines:   TDataReader, TDataSeq, TData
    */

    /* Requires */
    #define TTYPENAME   shape_change_cmdTYPENAME

    /* Defines */
    #define TDataReader shape_change_cmdDataReader
    #define TDataSeq    shape_change_cmdSeq
    #define TData       ShapeServ::shape_change_cmd

    #include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

    #undef TDataReader
    #undef TDataSeq
    #undef TData

    #undef TTYPENAME

    /* ----------------------------------------------------------------- */
    /* TypeSupport

    <<IMPLEMENTATION >>

    Requires:  TTYPENAME,
    TPlugin_new
    TPlugin_delete
    Defines:   TTypeSupport, TData, TDataReader, TDataWriter
    */

    /* Requires */
    #define TTYPENAME    shape_change_cmdTYPENAME
    #define TPlugin_new  ShapeServ::shape_change_cmdPlugin_new
    #define TPlugin_delete  ShapeServ::shape_change_cmdPlugin_delete

    /* Defines */
    #define TTypeSupport shape_change_cmdTypeSupport
    #define TData        ShapeServ::shape_change_cmd
    #define TDataReader  shape_change_cmdDataReader
    #define TDataWriter  shape_change_cmdDataWriter
    #define TGENERATE_SER_CODE
    #define TGENERATE_TYPECODE

    #include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

    #undef TTypeSupport
    #undef TData
    #undef TDataReader
    #undef TDataWriter
    #undef TGENERATE_TYPECODE
    #undef TGENERATE_SER_CODE
    #undef TTYPENAME
    #undef TPlugin_new
    #undef TPlugin_delete

    /* ========================================================================= */
    /**
    <<IMPLEMENTATION>>

    Defines:   TData,
    TDataWriter,
    TDataReader,
    TTypeSupport

    Configure and implement 'shape_change_cmd_wRequestor' support classes.

    Note: Only the #defined classes get defined
    */

    /* ----------------------------------------------------------------- */
    /* DDSDataWriter
    */

    /**
    <<IMPLEMENTATION >>

    Defines:   TDataWriter, TData
    */

    /* Requires */
    #define TTYPENAME   shape_change_cmd_wRequestorTYPENAME

    /* Defines */
    #define TDataWriter shape_change_cmd_wRequestorDataWriter
    #define TData       ShapeServ::shape_change_cmd_wRequestor

    #include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

    #undef TDataWriter
    #undef TData

    #undef TTYPENAME

    /* ----------------------------------------------------------------- */
    /* DDSDataReader
    */

    /**
    <<IMPLEMENTATION >>

    Defines:   TDataReader, TDataSeq, TData
    */

    /* Requires */
    #define TTYPENAME   shape_change_cmd_wRequestorTYPENAME

    /* Defines */
    #define TDataReader shape_change_cmd_wRequestorDataReader
    #define TDataSeq    shape_change_cmd_wRequestorSeq
    #define TData       ShapeServ::shape_change_cmd_wRequestor

    #include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

    #undef TDataReader
    #undef TDataSeq
    #undef TData

    #undef TTYPENAME

    /* ----------------------------------------------------------------- */
    /* TypeSupport

    <<IMPLEMENTATION >>

    Requires:  TTYPENAME,
    TPlugin_new
    TPlugin_delete
    Defines:   TTypeSupport, TData, TDataReader, TDataWriter
    */

    /* Requires */
    #define TTYPENAME    shape_change_cmd_wRequestorTYPENAME
    #define TPlugin_new  ShapeServ::shape_change_cmd_wRequestorPlugin_new
    #define TPlugin_delete  ShapeServ::shape_change_cmd_wRequestorPlugin_delete

    /* Defines */
    #define TTypeSupport shape_change_cmd_wRequestorTypeSupport
    #define TData        ShapeServ::shape_change_cmd_wRequestor
    #define TDataReader  shape_change_cmd_wRequestorDataReader
    #define TDataWriter  shape_change_cmd_wRequestorDataWriter
    #define TGENERATE_SER_CODE
    #define TGENERATE_TYPECODE

    #include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

    #undef TTypeSupport
    #undef TData
    #undef TDataReader
    #undef TDataWriter
    #undef TGENERATE_TYPECODE
    #undef TGENERATE_SER_CODE
    #undef TTYPENAME
    #undef TPlugin_new
    #undef TPlugin_delete

    /* ========================================================================= */
    /**
    <<IMPLEMENTATION>>

    Defines:   TData,
    TDataWriter,
    TDataReader,
    TTypeSupport

    Configure and implement 'shape_change_cmd_response' support classes.

    Note: Only the #defined classes get defined
    */

    /* ----------------------------------------------------------------- */
    /* DDSDataWriter
    */

    /**
    <<IMPLEMENTATION >>

    Defines:   TDataWriter, TData
    */

    /* Requires */
    #define TTYPENAME   shape_change_cmd_responseTYPENAME

    /* Defines */
    #define TDataWriter shape_change_cmd_responseDataWriter
    #define TData       ShapeServ::shape_change_cmd_response

    #include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

    #undef TDataWriter
    #undef TData

    #undef TTYPENAME

    /* ----------------------------------------------------------------- */
    /* DDSDataReader
    */

    /**
    <<IMPLEMENTATION >>

    Defines:   TDataReader, TDataSeq, TData
    */

    /* Requires */
    #define TTYPENAME   shape_change_cmd_responseTYPENAME

    /* Defines */
    #define TDataReader shape_change_cmd_responseDataReader
    #define TDataSeq    shape_change_cmd_responseSeq
    #define TData       ShapeServ::shape_change_cmd_response

    #include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

    #undef TDataReader
    #undef TDataSeq
    #undef TData

    #undef TTYPENAME

    /* ----------------------------------------------------------------- */
    /* TypeSupport

    <<IMPLEMENTATION >>

    Requires:  TTYPENAME,
    TPlugin_new
    TPlugin_delete
    Defines:   TTypeSupport, TData, TDataReader, TDataWriter
    */

    /* Requires */
    #define TTYPENAME    shape_change_cmd_responseTYPENAME
    #define TPlugin_new  ShapeServ::shape_change_cmd_responsePlugin_new
    #define TPlugin_delete  ShapeServ::shape_change_cmd_responsePlugin_delete

    /* Defines */
    #define TTypeSupport shape_change_cmd_responseTypeSupport
    #define TData        ShapeServ::shape_change_cmd_response
    #define TDataReader  shape_change_cmd_responseDataReader
    #define TDataWriter  shape_change_cmd_responseDataWriter
    #define TGENERATE_SER_CODE
    #define TGENERATE_TYPECODE

    #include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

    #undef TTypeSupport
    #undef TData
    #undef TDataReader
    #undef TDataWriter
    #undef TGENERATE_TYPECODE
    #undef TGENERATE_SER_CODE
    #undef TTYPENAME
    #undef TPlugin_new
    #undef TPlugin_delete

} /* namespace ShapeServ  */




/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from ShapeType.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

#ifndef cdr_type_h
#include "cdr/cdr_type.h"
#endif    

#ifndef osapi_heap_h
#include "osapi/osapi_heap.h" 
#endif
#else
#include "ndds_standalone_type.h"
#endif

#include "ShapeType.h"

/* ========================================================================= */
const char *ShapeFillKindTYPENAME = "ShapeFillKind";

DDS_TypeCode* ShapeFillKind_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode_Member ShapeFillKind_g_tc_members[4]=
    {

        {
            (char *)"SOLID_FILL",/* Member name */
            {
                0, /* Ignored */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            SOLID_FILL, /* Enumerator ordinal */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PRIVATE_MEMBER,/* Member visibility */ 

            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"TRANSPARENT_FILL",/* Member name */
            {
                0, /* Ignored */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            TRANSPARENT_FILL, /* Enumerator ordinal */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PRIVATE_MEMBER,/* Member visibility */ 

            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"HORIZONTAL_HATCH_FILL",/* Member name */
            {
                0, /* Ignored */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            HORIZONTAL_HATCH_FILL, /* Enumerator ordinal */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PRIVATE_MEMBER,/* Member visibility */ 

            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"VERTICAL_HATCH_FILL",/* Member name */
            {
                0, /* Ignored */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            VERTICAL_HATCH_FILL, /* Enumerator ordinal */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PRIVATE_MEMBER,/* Member visibility */ 

            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode ShapeFillKind_g_tc =
    {{
            DDS_TK_ENUM,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"ShapeFillKind", /* Name */
            NULL,     /* Base class type code is assigned later */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            4, /* Number of members */
            ShapeFillKind_g_tc_members, /* Members */
            DDS_VM_NONE   /* Type Modifier */        
        }}; /* Type code for ShapeFillKind*/

    if (is_initialized) {
        return &ShapeFillKind_g_tc;
    }

    is_initialized = RTI_TRUE;

    return &ShapeFillKind_g_tc;
}

RTIBool ShapeFillKind_initialize(
    ShapeFillKind* sample) {
    *sample = SOLID_FILL;
    return RTI_TRUE;
}

RTIBool ShapeFillKind_initialize_ex(
    ShapeFillKind* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return ShapeFillKind_initialize_w_params(
        sample,&allocParams);

}

RTIBool ShapeFillKind_initialize_w_params(
    ShapeFillKind* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (allocParams) {} /* To avoid warnings */
    *sample = SOLID_FILL;
    return RTI_TRUE;
}

void ShapeFillKind_finalize(
    ShapeFillKind* sample)
{

    if (sample==NULL) {
        return;
    }
}

void ShapeFillKind_finalize_ex(
    ShapeFillKind* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    ShapeFillKind_finalize_w_params(
        sample,&deallocParams);
}

void ShapeFillKind_finalize_w_params(
    ShapeFillKind* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */

}

void ShapeFillKind_finalize_optional_members(
    ShapeFillKind* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool ShapeFillKind_copy(
    ShapeFillKind* dst,
    const ShapeFillKind* src)
{

    return RTICdrType_copyEnum((RTICdrEnum *)dst, (RTICdrEnum *)src);

}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'ShapeFillKind' sequence class.
*/
#define T ShapeFillKind
#define TSeq ShapeFillKindSeq
#define T_initialize_w_params ShapeFillKind_initialize_w_params
#define T_finalize_w_params   ShapeFillKind_finalize_w_params
#define T_copy       ShapeFillKind_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T

/* ========================================================================= */
const char *ShapeTypeTYPENAME = "ShapeType";

DDS_TypeCode* ShapeType_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode ShapeType_g_tc_color_string = DDS_INITIALIZE_STRING_TYPECODE((128));
    static DDS_TypeCode_Member ShapeType_g_tc_members[4]=
    {

        {
            (char *)"color",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_KEY_MEMBER , /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"x",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"y",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"shapesize",/* Member name */
            {
                3,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode ShapeType_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"ShapeType", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            4, /* Number of members */
            ShapeType_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for ShapeType*/

    if (is_initialized) {
        return &ShapeType_g_tc;
    }

    ShapeType_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&ShapeType_g_tc_color_string;

    ShapeType_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    ShapeType_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    ShapeType_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    is_initialized = RTI_TRUE;

    return &ShapeType_g_tc;
}

RTIBool ShapeType_initialize(
    ShapeType* sample) {
    return ShapeType_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool ShapeType_initialize_ex(
    ShapeType* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return ShapeType_initialize_w_params(
        sample,&allocParams);

}

RTIBool ShapeType_initialize_w_params(
    ShapeType* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (allocParams) {} /* To avoid warnings */

    if (allocParams->allocate_memory){
        sample->color= DDS_String_alloc ((128));
        if (sample->color == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->color!= NULL) { 
            sample->color[0] = '\0';
        }
    }

    if (!RTICdrType_initLong(&sample->x)) {
        return RTI_FALSE;
    }     

    if (!RTICdrType_initLong(&sample->y)) {
        return RTI_FALSE;
    }     

    if (!RTICdrType_initLong(&sample->shapesize)) {
        return RTI_FALSE;
    }     

    return RTI_TRUE;
}

void ShapeType_finalize(
    ShapeType* sample)
{

    ShapeType_finalize_ex(sample,RTI_TRUE);
}

void ShapeType_finalize_ex(
    ShapeType* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    ShapeType_finalize_w_params(
        sample,&deallocParams);
}

void ShapeType_finalize_w_params(
    ShapeType* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */

    if (sample->color != NULL) {
        DDS_String_free(sample->color);
        sample->color=NULL;

    }

}

void ShapeType_finalize_optional_members(
    ShapeType* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool ShapeType_copy(
    ShapeType* dst,
    const ShapeType* src)
{

    if (!RTICdrType_copyStringEx (
        &dst->color, src->color, 
        (128) + 1, RTI_FALSE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->x, &src->x)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->y, &src->y)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->shapesize, &src->shapesize)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'ShapeType' sequence class.
*/
#define T ShapeType
#define TSeq ShapeTypeSeq
#define T_initialize_w_params ShapeType_initialize_w_params
#define T_finalize_w_params   ShapeType_finalize_w_params
#define T_copy       ShapeType_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T

/* ========================================================================= */
const char *ShapeTypeExtendedTYPENAME = "ShapeTypeExtended";

DDS_TypeCode* ShapeTypeExtended_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode_Member ShapeTypeExtended_g_tc_members[2]=
    {

        {
            (char *)"fillKind",/* Member name */
            {
                4,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"angle",/* Member name */
            {
                5,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode ShapeTypeExtended_g_tc =
    {{
            DDS_TK_VALUE,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"ShapeTypeExtended", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            2, /* Number of members */
            ShapeTypeExtended_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for ShapeTypeExtended*/

    if (is_initialized) {
        return &ShapeTypeExtended_g_tc;
    }

    ShapeTypeExtended_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)ShapeFillKind_get_typecode();

    ShapeTypeExtended_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_float;

    ShapeTypeExtended_g_tc._data._typeCode = (RTICdrTypeCode *)  ShapeType_get_typecode();  /* Base class */

    is_initialized = RTI_TRUE;

    return &ShapeTypeExtended_g_tc;
}

RTIBool ShapeTypeExtended_initialize(
    ShapeTypeExtended* sample) {
    return ShapeTypeExtended_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool ShapeTypeExtended_initialize_ex(
    ShapeTypeExtended* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return ShapeTypeExtended_initialize_w_params(
        sample,&allocParams);

}

RTIBool ShapeTypeExtended_initialize_w_params(
    ShapeTypeExtended* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (allocParams) {} /* To avoid warnings */
    if (!ShapeType_initialize_w_params((ShapeType*)sample,allocParams)) {
        return RTI_FALSE;
    }

    if (!ShapeFillKind_initialize_w_params(&sample->fillKind,
    allocParams)) {
        return RTI_FALSE;
    }

    if (!RTICdrType_initFloat(&sample->angle)) {
        return RTI_FALSE;
    }     

    return RTI_TRUE;
}

void ShapeTypeExtended_finalize(
    ShapeTypeExtended* sample)
{

    ShapeTypeExtended_finalize_ex(sample,RTI_TRUE);
}

void ShapeTypeExtended_finalize_ex(
    ShapeTypeExtended* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    ShapeTypeExtended_finalize_w_params(
        sample,&deallocParams);
}

void ShapeTypeExtended_finalize_w_params(
    ShapeTypeExtended* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */
    ShapeType_finalize_w_params((ShapeType*)sample,deallocParams);

    ShapeFillKind_finalize_w_params(&sample->fillKind,deallocParams);

}

void ShapeTypeExtended_finalize_optional_members(
    ShapeTypeExtended* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    ShapeType_finalize_optional_members((ShapeType*)sample,deletePointers);

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

    ShapeFillKind_finalize_optional_members(&sample->fillKind, deallocParams->delete_pointers);
}

RTIBool ShapeTypeExtended_copy(
    ShapeTypeExtended* dst,
    const ShapeTypeExtended* src)
{

    if(!ShapeType_copy((ShapeType*)dst,(ShapeType*)src)) {
        return RTI_FALSE;
    }

    if (!ShapeFillKind_copy(
        &dst->fillKind, &src->fillKind)) {
        return RTI_FALSE;
    } 
    if (!RTICdrType_copyFloat (
        &dst->angle, &src->angle)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'ShapeTypeExtended' sequence class.
*/
#define T ShapeTypeExtended
#define TSeq ShapeTypeExtendedSeq
#define T_initialize_w_params ShapeTypeExtended_initialize_w_params
#define T_finalize_w_params   ShapeTypeExtended_finalize_w_params
#define T_copy       ShapeTypeExtended_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T
namespace ShapeServ {

    /* ========================================================================= */

    DDS_TypeCode* size_t_get_typecode()
    {
        static RTIBool is_initialized = RTI_FALSE;

        static DDS_TypeCode size_t_g_tc =
        {{
                DDS_TK_ALIAS, /* Kind*/
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Ignored */
                (char *)"ShapeServ::size_t", /* Name */
                NULL, /* Content type code is assigned later */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                DDS_VM_NONE /* Ignored */
            }}; /* Type code for  size_t */

        if (is_initialized) {
            return &size_t_g_tc;
        }

        size_t_g_tc._data._typeCode =  (RTICdrTypeCode *)&DDS_g_tc_long;

        is_initialized = RTI_TRUE;

        return &size_t_g_tc;
    }

    RTIBool size_t_initialize(
        size_t* sample) {
        return ShapeServ::size_t_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
    }

    RTIBool size_t_initialize_ex(
        size_t* sample,RTIBool allocatePointers, RTIBool allocateMemory)
    {

        struct DDS_TypeAllocationParams_t allocParams =
        DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

        allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
        allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

        return ShapeServ::size_t_initialize_w_params(
            sample,&allocParams);

    }

    RTIBool size_t_initialize_w_params(
        size_t* sample, const struct DDS_TypeAllocationParams_t * allocParams)
    {

        if (allocParams) {} /* To avoid warnings */

        if (!RTICdrType_initLong(sample)) {
            return RTI_FALSE;
        }     

        return RTI_TRUE;
    }

    void size_t_finalize(
        size_t* sample)
    {

        ShapeServ::size_t_finalize_ex(sample,RTI_TRUE);
    }

    void size_t_finalize_ex(
        size_t* sample,RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParams =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

        if (sample==NULL) {
            return;
        } 

        deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

        ShapeServ::size_t_finalize_w_params(
            sample,&deallocParams);
    }

    void size_t_finalize_w_params(
        size_t* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
    {

        if (sample==NULL) {
            return;
        }
        if (deallocParams) {} /* To avoid warnings */

    }

    void size_t_finalize_optional_members(
        size_t* sample, RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParamsTmp =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
        struct DDS_TypeDeallocationParams_t * deallocParams =
        &deallocParamsTmp;

        if (sample==NULL) {
            return;
        } 
        if (deallocParams) {} /* To avoid warnings */

        deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
        deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

    }

    RTIBool size_t_copy(
        size_t* dst,
        const size_t* src)
    {

        if (!RTICdrType_copyLong (
            dst, src)) { 
            return RTI_FALSE;
        }

        return RTI_TRUE;
    }

    /**
    * <<IMPLEMENTATION>>
    *
    * Defines:  TSeq, T
    *
    * Configure and implement 'size_t' sequence class.
    */
    #define T size_t
    #define TSeq size_tSeq
    #define T_initialize_w_params ShapeServ::size_t_initialize_w_params
    #define T_finalize_w_params   ShapeServ::size_t_finalize_w_params
    #define T_copy       ShapeServ::size_t_copy

    #ifndef NDDS_STANDALONE_TYPE
    #include "dds_c/generic/dds_c_sequence_TSeq.gen"
    #include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
    #else
    #include "dds_c_sequence_TSeq.gen"
    #include "dds_cpp_sequence_TSeq.gen"
    #endif

    #undef T_copy
    #undef T_finalize_w_params
    #undef T_initialize_w_params
    #undef TSeq
    #undef T

    /* ========================================================================= */
    const char *cmd_tTYPENAME = "ShapeServ::cmd_t";

    DDS_TypeCode* cmd_t_get_typecode()
    {
        static RTIBool is_initialized = RTI_FALSE;

        static DDS_TypeCode_Member cmd_t_g_tc_members[2]=
        {

            {
                (char *)"FILL",/* Member name */
                {
                    0, /* Ignored */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                FILL, /* Enumerator ordinal */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PRIVATE_MEMBER,/* Member visibility */ 

                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"ANGLE",/* Member name */
                {
                    0, /* Ignored */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                ANGLE, /* Enumerator ordinal */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PRIVATE_MEMBER,/* Member visibility */ 

                1,
                NULL/* Ignored */
            }
        };

        static DDS_TypeCode cmd_t_g_tc =
        {{
                DDS_TK_ENUM,/* Kind */
                DDS_BOOLEAN_FALSE, /* Ignored */
                -1, /*Ignored*/
                (char *)"ShapeServ::cmd_t", /* Name */
                NULL,     /* Base class type code is assigned later */      
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                2, /* Number of members */
                cmd_t_g_tc_members, /* Members */
                DDS_VM_NONE   /* Type Modifier */        
            }}; /* Type code for cmd_t*/

        if (is_initialized) {
            return &cmd_t_g_tc;
        }

        is_initialized = RTI_TRUE;

        return &cmd_t_g_tc;
    }

    RTIBool cmd_t_initialize(
        cmd_t* sample) {
        *sample = FILL;
        return RTI_TRUE;
    }

    RTIBool cmd_t_initialize_ex(
        cmd_t* sample,RTIBool allocatePointers, RTIBool allocateMemory)
    {

        struct DDS_TypeAllocationParams_t allocParams =
        DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

        allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
        allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

        return ShapeServ::cmd_t_initialize_w_params(
            sample,&allocParams);

    }

    RTIBool cmd_t_initialize_w_params(
        cmd_t* sample, const struct DDS_TypeAllocationParams_t * allocParams)
    {

        if (allocParams) {} /* To avoid warnings */
        *sample = FILL;
        return RTI_TRUE;
    }

    void cmd_t_finalize(
        cmd_t* sample)
    {

        if (sample==NULL) {
            return;
        }
    }

    void cmd_t_finalize_ex(
        cmd_t* sample,RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParams =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

        if (sample==NULL) {
            return;
        } 

        deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

        ShapeServ::cmd_t_finalize_w_params(
            sample,&deallocParams);
    }

    void cmd_t_finalize_w_params(
        cmd_t* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
    {

        if (sample==NULL) {
            return;
        }
        if (deallocParams) {} /* To avoid warnings */

    }

    void cmd_t_finalize_optional_members(
        cmd_t* sample, RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParamsTmp =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
        struct DDS_TypeDeallocationParams_t * deallocParams =
        &deallocParamsTmp;

        if (sample==NULL) {
            return;
        } 
        if (deallocParams) {} /* To avoid warnings */

        deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
        deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

    }

    RTIBool cmd_t_copy(
        cmd_t* dst,
        const cmd_t* src)
    {

        return RTICdrType_copyEnum((RTICdrEnum *)dst, (RTICdrEnum *)src);

    }

    /**
    * <<IMPLEMENTATION>>
    *
    * Defines:  TSeq, T
    *
    * Configure and implement 'cmd_t' sequence class.
    */
    #define T cmd_t
    #define TSeq cmd_tSeq
    #define T_initialize_w_params ShapeServ::cmd_t_initialize_w_params
    #define T_finalize_w_params   ShapeServ::cmd_t_finalize_w_params
    #define T_copy       ShapeServ::cmd_t_copy

    #ifndef NDDS_STANDALONE_TYPE
    #include "dds_c/generic/dds_c_sequence_TSeq.gen"
    #include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
    #else
    #include "dds_c_sequence_TSeq.gen"
    #include "dds_cpp_sequence_TSeq.gen"
    #endif

    #undef T_copy
    #undef T_finalize_w_params
    #undef T_initialize_w_params
    #undef TSeq
    #undef T

    /* ========================================================================= */
    const char *cmd_typeTYPENAME = "ShapeServ::cmd_type";

    DDS_TypeCode* cmd_type_get_typecode()
    {
        static RTIBool is_initialized = RTI_FALSE;

        static DDS_TypeCode_Member cmd_type_g_tc_members[2]=
        {

            {
                (char *)"fill",/* Member name */
                {
                    1,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                1, /* Number of labels */
                (ShapeServ::FILL), /* First label */
                NULL, /* Labels (it is NULL when there is only one label)*/
                RTI_CDR_NONKEY_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"angle",/* Member name */
                {
                    2,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                1, /* Number of labels */
                (ShapeServ::ANGLE), /* First label */
                NULL, /* Labels (it is NULL when there is only one label)*/
                RTI_CDR_NONKEY_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }
        };

        static DDS_TypeCode cmd_type_g_tc =
        {{
                DDS_TK_UNION,/* Kind */
                DDS_BOOLEAN_FALSE, /* Ignored */
                -1, /*Ignored*/
                (char *)"ShapeServ::cmd_type", /* Name */
                NULL,     /* Base class type code is assigned later */      
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                2, /* Number of members */
                cmd_type_g_tc_members, /* Members */
                DDS_VM_NONE   /* Type Modifier */        
            }}; /* Type code for cmd_type*/

        if (is_initialized) {
            return &cmd_type_g_tc;
        }

        cmd_type_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)ShapeFillKind_get_typecode();

        cmd_type_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_float;

        /* Discriminator type code */
        cmd_type_g_tc._data._typeCode = (RTICdrTypeCode *)ShapeServ::cmd_t_get_typecode();

        is_initialized = RTI_TRUE;

        return &cmd_type_g_tc;
    }

    DDS_LongLong cmd_type_getDefaultDiscriminator(){

        return 0;
    }

    RTIBool cmd_type_initialize(
        cmd_type* sample) {
        return ShapeServ::cmd_type_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
    }

    RTIBool cmd_type_initialize_ex(
        cmd_type* sample,RTIBool allocatePointers, RTIBool allocateMemory)
    {

        struct DDS_TypeAllocationParams_t allocParams =
        DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

        allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
        allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

        return ShapeServ::cmd_type_initialize_w_params(
            sample,&allocParams);

    }

    RTIBool cmd_type_initialize_w_params(
        cmd_type* sample, const struct DDS_TypeAllocationParams_t * allocParams)
    {

        if (allocParams) {} /* To avoid warnings */

        sample->_d = (ShapeServ::cmd_t)cmd_type_getDefaultDiscriminator();
        if (!ShapeFillKind_initialize_w_params(&sample->_u.fill,
        allocParams)) {
            return RTI_FALSE;
        }

        if (!RTICdrType_initFloat(&sample->_u.angle)) {
            return RTI_FALSE;
        }     

        return RTI_TRUE;
    }

    void cmd_type_finalize(
        cmd_type* sample)
    {

        ShapeServ::cmd_type_finalize_ex(sample,RTI_TRUE);
    }

    void cmd_type_finalize_ex(
        cmd_type* sample,RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParams =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

        if (sample==NULL) {
            return;
        } 

        deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

        ShapeServ::cmd_type_finalize_w_params(
            sample,&deallocParams);
    }

    void cmd_type_finalize_w_params(
        cmd_type* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
    {

        if (sample==NULL) {
            return;
        }
        if (deallocParams) {} /* To avoid warnings */

        ShapeFillKind_finalize_w_params(&sample->_u.fill,deallocParams);

    }

    void cmd_type_finalize_optional_members(
        cmd_type* sample, RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParamsTmp =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
        struct DDS_TypeDeallocationParams_t * deallocParams =
        &deallocParamsTmp;

        if (sample==NULL) {
            return;
        } 
        if (deallocParams) {} /* To avoid warnings */

        deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
        deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

        switch(sample->_d) {
            case (ShapeServ::FILL):
                {  
                    ShapeFillKind_finalize_optional_members(&sample->_u.fill, deallocParams->delete_pointers);
            } break ;
            case (ShapeServ::ANGLE):
                {  
            } break ;
        }
    }

    RTIBool cmd_type_copy(
        cmd_type* dst,
        const cmd_type* src)
    {

        if (!ShapeServ::cmd_t_copy(
            &dst->_d, &src->_d)) {
            return RTI_FALSE;
        } 

        switch(src->_d) {

            case (ShapeServ::FILL):
                {  
                    if (!ShapeFillKind_copy(
                        &dst->_u.fill, &src->_u.fill)) {
                        return RTI_FALSE;
                } 
            } break ;
            case (ShapeServ::ANGLE):
                {  
                    if (!RTICdrType_copyFloat (
                        &dst->_u.angle, &src->_u.angle)) { 
                        return RTI_FALSE;
                }
            } break ;

        }
        return RTI_TRUE;
    }

    /**
    * <<IMPLEMENTATION>>
    *
    * Defines:  TSeq, T
    *
    * Configure and implement 'cmd_type' sequence class.
    */
    #define T cmd_type
    #define TSeq cmd_typeSeq
    #define T_initialize_w_params ShapeServ::cmd_type_initialize_w_params
    #define T_finalize_w_params   ShapeServ::cmd_type_finalize_w_params
    #define T_copy       ShapeServ::cmd_type_copy

    #ifndef NDDS_STANDALONE_TYPE
    #include "dds_c/generic/dds_c_sequence_TSeq.gen"
    #include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
    #else
    #include "dds_c_sequence_TSeq.gen"
    #include "dds_cpp_sequence_TSeq.gen"
    #endif

    #undef T_copy
    #undef T_finalize_w_params
    #undef T_initialize_w_params
    #undef TSeq
    #undef T

    /* ========================================================================= */
    const char *shape_change_cmdTYPENAME = "ShapeServ::shape_change_cmd";

    DDS_TypeCode* shape_change_cmd_get_typecode()
    {
        static RTIBool is_initialized = RTI_FALSE;

        static DDS_TypeCode_Member shape_change_cmd_g_tc_members[3]=
        {

            {
                (char *)"sensor_id",/* Member name */
                {
                    0,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_KEY_MEMBER , /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"size",/* Member name */
                {
                    1,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"command",/* Member name */
                {
                    2,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }
        };

        static DDS_TypeCode shape_change_cmd_g_tc =
        {{
                DDS_TK_STRUCT,/* Kind */
                DDS_BOOLEAN_FALSE, /* Ignored */
                -1, /*Ignored*/
                (char *)"ShapeServ::shape_change_cmd", /* Name */
                NULL, /* Ignored */      
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                3, /* Number of members */
                shape_change_cmd_g_tc_members, /* Members */
                DDS_VM_NONE  /* Ignored */         
            }}; /* Type code for shape_change_cmd*/

        if (is_initialized) {
            return &shape_change_cmd_g_tc;
        }

        shape_change_cmd_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

        shape_change_cmd_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)ShapeServ::size_t_get_typecode();

        shape_change_cmd_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)ShapeServ::cmd_type_get_typecode();

        is_initialized = RTI_TRUE;

        return &shape_change_cmd_g_tc;
    }

    RTIBool shape_change_cmd_initialize(
        shape_change_cmd* sample) {
        return ShapeServ::shape_change_cmd_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
    }

    RTIBool shape_change_cmd_initialize_ex(
        shape_change_cmd* sample,RTIBool allocatePointers, RTIBool allocateMemory)
    {

        struct DDS_TypeAllocationParams_t allocParams =
        DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

        allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
        allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

        return ShapeServ::shape_change_cmd_initialize_w_params(
            sample,&allocParams);

    }

    RTIBool shape_change_cmd_initialize_w_params(
        shape_change_cmd* sample, const struct DDS_TypeAllocationParams_t * allocParams)
    {

        if (allocParams) {} /* To avoid warnings */

        if (!RTICdrType_initLong(&sample->sensor_id)) {
            return RTI_FALSE;
        }     

        if (!ShapeServ::size_t_initialize_w_params(&sample->size,
        allocParams)) {
            return RTI_FALSE;
        }
        if (!ShapeServ::cmd_type_initialize_w_params(&sample->command,
        allocParams)) {
            return RTI_FALSE;
        }
        return RTI_TRUE;
    }

    void shape_change_cmd_finalize(
        shape_change_cmd* sample)
    {

        ShapeServ::shape_change_cmd_finalize_ex(sample,RTI_TRUE);
    }

    void shape_change_cmd_finalize_ex(
        shape_change_cmd* sample,RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParams =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

        if (sample==NULL) {
            return;
        } 

        deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

        ShapeServ::shape_change_cmd_finalize_w_params(
            sample,&deallocParams);
    }

    void shape_change_cmd_finalize_w_params(
        shape_change_cmd* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
    {

        if (sample==NULL) {
            return;
        }
        if (deallocParams) {} /* To avoid warnings */

        ShapeServ::size_t_finalize_w_params(&sample->size,deallocParams);

        ShapeServ::cmd_type_finalize_w_params(&sample->command,deallocParams);

    }

    void shape_change_cmd_finalize_optional_members(
        shape_change_cmd* sample, RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParamsTmp =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
        struct DDS_TypeDeallocationParams_t * deallocParams =
        &deallocParamsTmp;

        if (sample==NULL) {
            return;
        } 
        if (deallocParams) {} /* To avoid warnings */

        deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
        deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

        ShapeServ::size_t_finalize_optional_members(&sample->size, deallocParams->delete_pointers);
        ShapeServ::cmd_type_finalize_optional_members(&sample->command, deallocParams->delete_pointers);
    }

    RTIBool shape_change_cmd_copy(
        shape_change_cmd* dst,
        const shape_change_cmd* src)
    {

        if (!RTICdrType_copyLong (
            &dst->sensor_id, &src->sensor_id)) { 
            return RTI_FALSE;
        }
        if (!ShapeServ::size_t_copy(
            &dst->size, &src->size)) {
            return RTI_FALSE;
        } 
        if (!ShapeServ::cmd_type_copy(
            &dst->command, &src->command)) {
            return RTI_FALSE;
        } 

        return RTI_TRUE;
    }

    /**
    * <<IMPLEMENTATION>>
    *
    * Defines:  TSeq, T
    *
    * Configure and implement 'shape_change_cmd' sequence class.
    */
    #define T shape_change_cmd
    #define TSeq shape_change_cmdSeq
    #define T_initialize_w_params ShapeServ::shape_change_cmd_initialize_w_params
    #define T_finalize_w_params   ShapeServ::shape_change_cmd_finalize_w_params
    #define T_copy       ShapeServ::shape_change_cmd_copy

    #ifndef NDDS_STANDALONE_TYPE
    #include "dds_c/generic/dds_c_sequence_TSeq.gen"
    #include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
    #else
    #include "dds_c_sequence_TSeq.gen"
    #include "dds_cpp_sequence_TSeq.gen"
    #endif

    #undef T_copy
    #undef T_finalize_w_params
    #undef T_initialize_w_params
    #undef TSeq
    #undef T

    /* ========================================================================= */
    const char *shape_change_cmd_wRequestorTYPENAME = "ShapeServ::shape_change_cmd_wRequestor";

    DDS_TypeCode* shape_change_cmd_wRequestor_get_typecode()
    {
        static RTIBool is_initialized = RTI_FALSE;

        static DDS_TypeCode_Member shape_change_cmd_wRequestor_g_tc_members[1]=
        {

            {
                (char *)"requestor_id",/* Member name */
                {
                    3,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }
        };

        static DDS_TypeCode shape_change_cmd_wRequestor_g_tc =
        {{
                DDS_TK_VALUE,/* Kind */
                DDS_BOOLEAN_FALSE, /* Ignored */
                -1, /*Ignored*/
                (char *)"ShapeServ::shape_change_cmd_wRequestor", /* Name */
                NULL, /* Ignored */      
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                1, /* Number of members */
                shape_change_cmd_wRequestor_g_tc_members, /* Members */
                DDS_VM_NONE  /* Ignored */         
            }}; /* Type code for shape_change_cmd_wRequestor*/

        if (is_initialized) {
            return &shape_change_cmd_wRequestor_g_tc;
        }

        shape_change_cmd_wRequestor_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

        shape_change_cmd_wRequestor_g_tc._data._typeCode = (RTICdrTypeCode *)  ShapeServ::shape_change_cmd_get_typecode();  /* Base class */

        is_initialized = RTI_TRUE;

        return &shape_change_cmd_wRequestor_g_tc;
    }

    RTIBool shape_change_cmd_wRequestor_initialize(
        shape_change_cmd_wRequestor* sample) {
        return ShapeServ::shape_change_cmd_wRequestor_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
    }

    RTIBool shape_change_cmd_wRequestor_initialize_ex(
        shape_change_cmd_wRequestor* sample,RTIBool allocatePointers, RTIBool allocateMemory)
    {

        struct DDS_TypeAllocationParams_t allocParams =
        DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

        allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
        allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

        return ShapeServ::shape_change_cmd_wRequestor_initialize_w_params(
            sample,&allocParams);

    }

    RTIBool shape_change_cmd_wRequestor_initialize_w_params(
        shape_change_cmd_wRequestor* sample, const struct DDS_TypeAllocationParams_t * allocParams)
    {

        if (allocParams) {} /* To avoid warnings */
        if (!ShapeServ::shape_change_cmd_initialize_w_params((ShapeServ::shape_change_cmd*)sample,allocParams)) {
            return RTI_FALSE;
        }

        if (!RTICdrType_initLong(&sample->requestor_id)) {
            return RTI_FALSE;
        }     

        return RTI_TRUE;
    }

    void shape_change_cmd_wRequestor_finalize(
        shape_change_cmd_wRequestor* sample)
    {

        ShapeServ::shape_change_cmd_wRequestor_finalize_ex(sample,RTI_TRUE);
    }

    void shape_change_cmd_wRequestor_finalize_ex(
        shape_change_cmd_wRequestor* sample,RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParams =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

        if (sample==NULL) {
            return;
        } 

        deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

        ShapeServ::shape_change_cmd_wRequestor_finalize_w_params(
            sample,&deallocParams);
    }

    void shape_change_cmd_wRequestor_finalize_w_params(
        shape_change_cmd_wRequestor* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
    {

        if (sample==NULL) {
            return;
        }
        if (deallocParams) {} /* To avoid warnings */
        ShapeServ::shape_change_cmd_finalize_w_params((ShapeServ::shape_change_cmd*)sample,deallocParams);

    }

    void shape_change_cmd_wRequestor_finalize_optional_members(
        shape_change_cmd_wRequestor* sample, RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParamsTmp =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
        struct DDS_TypeDeallocationParams_t * deallocParams =
        &deallocParamsTmp;

        if (sample==NULL) {
            return;
        } 
        if (deallocParams) {} /* To avoid warnings */

        ShapeServ::shape_change_cmd_finalize_optional_members((ShapeServ::shape_change_cmd*)sample,deletePointers);

        deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
        deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

    }

    RTIBool shape_change_cmd_wRequestor_copy(
        shape_change_cmd_wRequestor* dst,
        const shape_change_cmd_wRequestor* src)
    {

        if(!ShapeServ::shape_change_cmd_copy((ShapeServ::shape_change_cmd*)dst,(ShapeServ::shape_change_cmd*)src)) {
            return RTI_FALSE;
        }

        if (!RTICdrType_copyLong (
            &dst->requestor_id, &src->requestor_id)) { 
            return RTI_FALSE;
        }

        return RTI_TRUE;
    }

    /**
    * <<IMPLEMENTATION>>
    *
    * Defines:  TSeq, T
    *
    * Configure and implement 'shape_change_cmd_wRequestor' sequence class.
    */
    #define T shape_change_cmd_wRequestor
    #define TSeq shape_change_cmd_wRequestorSeq
    #define T_initialize_w_params ShapeServ::shape_change_cmd_wRequestor_initialize_w_params
    #define T_finalize_w_params   ShapeServ::shape_change_cmd_wRequestor_finalize_w_params
    #define T_copy       ShapeServ::shape_change_cmd_wRequestor_copy

    #ifndef NDDS_STANDALONE_TYPE
    #include "dds_c/generic/dds_c_sequence_TSeq.gen"
    #include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
    #else
    #include "dds_c_sequence_TSeq.gen"
    #include "dds_cpp_sequence_TSeq.gen"
    #endif

    #undef T_copy
    #undef T_finalize_w_params
    #undef T_initialize_w_params
    #undef TSeq
    #undef T

    /* ========================================================================= */
    const char *shape_change_cmd_responseTYPENAME = "ShapeServ::shape_change_cmd_response";

    DDS_TypeCode* shape_change_cmd_response_get_typecode()
    {
        static RTIBool is_initialized = RTI_FALSE;

        static DDS_TypeCode_Member shape_change_cmd_response_g_tc_members[5]=
        {

            {
                (char *)"requestor_id",/* Member name */
                {
                    0,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_KEY_MEMBER , /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"sensor_id",/* Member name */
                {
                    1,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_KEY_MEMBER , /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"commanded_command",/* Member name */
                {
                    2,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"commanded_size",/* Member name */
                {
                    3,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"isValid",/* Member name */
                {
                    4,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }
        };

        static DDS_TypeCode shape_change_cmd_response_g_tc =
        {{
                DDS_TK_STRUCT,/* Kind */
                DDS_BOOLEAN_FALSE, /* Ignored */
                -1, /*Ignored*/
                (char *)"ShapeServ::shape_change_cmd_response", /* Name */
                NULL, /* Ignored */      
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                5, /* Number of members */
                shape_change_cmd_response_g_tc_members, /* Members */
                DDS_VM_NONE  /* Ignored */         
            }}; /* Type code for shape_change_cmd_response*/

        if (is_initialized) {
            return &shape_change_cmd_response_g_tc;
        }

        shape_change_cmd_response_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

        shape_change_cmd_response_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

        shape_change_cmd_response_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)ShapeServ::cmd_type_get_typecode();

        shape_change_cmd_response_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)ShapeServ::size_t_get_typecode();

        shape_change_cmd_response_g_tc_members[4]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_boolean;

        is_initialized = RTI_TRUE;

        return &shape_change_cmd_response_g_tc;
    }

    RTIBool shape_change_cmd_response_initialize(
        shape_change_cmd_response* sample) {
        return ShapeServ::shape_change_cmd_response_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
    }

    RTIBool shape_change_cmd_response_initialize_ex(
        shape_change_cmd_response* sample,RTIBool allocatePointers, RTIBool allocateMemory)
    {

        struct DDS_TypeAllocationParams_t allocParams =
        DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

        allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
        allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

        return ShapeServ::shape_change_cmd_response_initialize_w_params(
            sample,&allocParams);

    }

    RTIBool shape_change_cmd_response_initialize_w_params(
        shape_change_cmd_response* sample, const struct DDS_TypeAllocationParams_t * allocParams)
    {

        if (allocParams) {} /* To avoid warnings */

        if (!RTICdrType_initLong(&sample->requestor_id)) {
            return RTI_FALSE;
        }     

        if (!RTICdrType_initLong(&sample->sensor_id)) {
            return RTI_FALSE;
        }     

        if (!ShapeServ::cmd_type_initialize_w_params(&sample->commanded_command,
        allocParams)) {
            return RTI_FALSE;
        }
        if (!ShapeServ::size_t_initialize_w_params(&sample->commanded_size,
        allocParams)) {
            return RTI_FALSE;
        }

        if (!RTICdrType_initBoolean(&sample->isValid)) {
            return RTI_FALSE;
        }     

        return RTI_TRUE;
    }

    void shape_change_cmd_response_finalize(
        shape_change_cmd_response* sample)
    {

        ShapeServ::shape_change_cmd_response_finalize_ex(sample,RTI_TRUE);
    }

    void shape_change_cmd_response_finalize_ex(
        shape_change_cmd_response* sample,RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParams =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

        if (sample==NULL) {
            return;
        } 

        deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

        ShapeServ::shape_change_cmd_response_finalize_w_params(
            sample,&deallocParams);
    }

    void shape_change_cmd_response_finalize_w_params(
        shape_change_cmd_response* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
    {

        if (sample==NULL) {
            return;
        }
        if (deallocParams) {} /* To avoid warnings */

        ShapeServ::cmd_type_finalize_w_params(&sample->commanded_command,deallocParams);

        ShapeServ::size_t_finalize_w_params(&sample->commanded_size,deallocParams);

    }

    void shape_change_cmd_response_finalize_optional_members(
        shape_change_cmd_response* sample, RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParamsTmp =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
        struct DDS_TypeDeallocationParams_t * deallocParams =
        &deallocParamsTmp;

        if (sample==NULL) {
            return;
        } 
        if (deallocParams) {} /* To avoid warnings */

        deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
        deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

        ShapeServ::cmd_type_finalize_optional_members(&sample->commanded_command, deallocParams->delete_pointers);
        ShapeServ::size_t_finalize_optional_members(&sample->commanded_size, deallocParams->delete_pointers);
    }

    RTIBool shape_change_cmd_response_copy(
        shape_change_cmd_response* dst,
        const shape_change_cmd_response* src)
    {

        if (!RTICdrType_copyLong (
            &dst->requestor_id, &src->requestor_id)) { 
            return RTI_FALSE;
        }
        if (!RTICdrType_copyLong (
            &dst->sensor_id, &src->sensor_id)) { 
            return RTI_FALSE;
        }
        if (!ShapeServ::cmd_type_copy(
            &dst->commanded_command, &src->commanded_command)) {
            return RTI_FALSE;
        } 
        if (!ShapeServ::size_t_copy(
            &dst->commanded_size, &src->commanded_size)) {
            return RTI_FALSE;
        } 
        if (!RTICdrType_copyBoolean (
            &dst->isValid, &src->isValid)) { 
            return RTI_FALSE;
        }

        return RTI_TRUE;
    }

    /**
    * <<IMPLEMENTATION>>
    *
    * Defines:  TSeq, T
    *
    * Configure and implement 'shape_change_cmd_response' sequence class.
    */
    #define T shape_change_cmd_response
    #define TSeq shape_change_cmd_responseSeq
    #define T_initialize_w_params ShapeServ::shape_change_cmd_response_initialize_w_params
    #define T_finalize_w_params   ShapeServ::shape_change_cmd_response_finalize_w_params
    #define T_copy       ShapeServ::shape_change_cmd_response_copy

    #ifndef NDDS_STANDALONE_TYPE
    #include "dds_c/generic/dds_c_sequence_TSeq.gen"
    #include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
    #else
    #include "dds_c_sequence_TSeq.gen"
    #include "dds_cpp_sequence_TSeq.gen"
    #endif

    #undef T_copy
    #undef T_finalize_w_params
    #undef T_initialize_w_params
    #undef TSeq
    #undef T
} /* namespace ShapeServ  */


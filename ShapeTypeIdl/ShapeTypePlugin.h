

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from ShapeType.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef ShapeTypePlugin_434675311_h
#define ShapeTypePlugin_434675311_h

#include "ShapeType.h"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
ShapeFillKindPlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const ShapeFillKind *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeFillKindPlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeFillKind *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
ShapeFillKindPlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
ShapeFillKindPlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
ShapeFillKindPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
ShapeFillKindPlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
ShapeFillKindPlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const ShapeFillKind * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern unsigned int 
ShapeFillKindPlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
ShapeFillKindPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
ShapeFillKindPlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const ShapeFillKind *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeFillKindPlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeFillKind * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
ShapeFillKindPlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    ShapeFillKind *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

/* ----------------------------------------------------------------------------
Support functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern void
ShapeFillKindPluginSupport_print_data(
    const ShapeFillKind *sample, const char *desc, int indent_level);

/* The type used to store keys for instances of type struct
* AnotherSimple.
*
* By default, this type is struct ShapeType
* itself. However, if for some reason this choice is not practical for your
* system (e.g. if sizeof(struct ShapeType)
* is very large), you may redefine this typedef in terms of another type of
* your choosing. HOWEVER, if you define the KeyHolder type to be something
* other than struct AnotherSimple, the
* following restriction applies: the key of struct
* ShapeType must consist of a
* single field of your redefined KeyHolder type and that field must be the
* first field in struct ShapeType.
*/
typedef  class ShapeType ShapeTypeKeyHolder;

#define ShapeTypePlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
#define ShapeTypePlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define ShapeTypePlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

#define ShapeTypePlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
#define ShapeTypePlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

#define ShapeTypePlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define ShapeTypePlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern ShapeType*
ShapeTypePluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern ShapeType*
ShapeTypePluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern ShapeType*
ShapeTypePluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
ShapeTypePluginSupport_copy_data(
    ShapeType *out,
    const ShapeType *in);

NDDSUSERDllExport extern void 
ShapeTypePluginSupport_destroy_data_w_params(
    ShapeType *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
ShapeTypePluginSupport_destroy_data_ex(
    ShapeType *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
ShapeTypePluginSupport_destroy_data(
    ShapeType *sample);

NDDSUSERDllExport extern void 
ShapeTypePluginSupport_print_data(
    const ShapeType *sample,
    const char *desc,
    unsigned int indent);

NDDSUSERDllExport extern ShapeType*
ShapeTypePluginSupport_create_key_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern ShapeType*
ShapeTypePluginSupport_create_key(void);

NDDSUSERDllExport extern void 
ShapeTypePluginSupport_destroy_key_ex(
    ShapeTypeKeyHolder *key,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
ShapeTypePluginSupport_destroy_key(
    ShapeTypeKeyHolder *key);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
ShapeTypePlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
ShapeTypePlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
ShapeTypePlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
ShapeTypePlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
ShapeTypePlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeType *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeType *out,
    const ShapeType *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const ShapeType *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeType *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
ShapeTypePlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const ShapeType *sample); 

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    ShapeType **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
ShapeTypePlugin_deserialize_from_cdr_buffer(
    ShapeType *sample,
    const char * buffer,
    unsigned int length);    

NDDSUSERDllExport extern RTIBool
ShapeTypePlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
ShapeTypePlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
ShapeTypePlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
ShapeTypePlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
ShapeTypePlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const ShapeType * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
ShapeTypePlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
ShapeTypePlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
ShapeTypePlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const ShapeType *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeType * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    ShapeType ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
ShapeTypePlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    ShapeType *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_instance_to_key(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeKeyHolder *key, 
    const ShapeType *instance);

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_key_to_instance(
    PRESTypePluginEndpointData endpoint_data,
    ShapeType *instance, 
    const ShapeTypeKeyHolder *key);

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_instance_to_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    DDS_KeyHash_t *keyhash,
    const ShapeType *instance);

NDDSUSERDllExport extern RTIBool 
ShapeTypePlugin_serialized_sample_to_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    DDS_KeyHash_t *keyhash,
    RTIBool deserialize_encapsulation,
    void *endpoint_plugin_qos); 

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
ShapeTypePlugin_new(void);

NDDSUSERDllExport extern void
ShapeTypePlugin_delete(struct PRESTypePlugin *);

/* The type used to store keys for instances of type struct
* AnotherSimple.
*
* By default, this type is struct ShapeTypeExtended
* itself. However, if for some reason this choice is not practical for your
* system (e.g. if sizeof(struct ShapeTypeExtended)
* is very large), you may redefine this typedef in terms of another type of
* your choosing. HOWEVER, if you define the KeyHolder type to be something
* other than struct AnotherSimple, the
* following restriction applies: the key of struct
* ShapeTypeExtended must consist of a
* single field of your redefined KeyHolder type and that field must be the
* first field in struct ShapeTypeExtended.
*/
typedef  class ShapeTypeExtended ShapeTypeExtendedKeyHolder;

#define ShapeTypeExtendedPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
#define ShapeTypeExtendedPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define ShapeTypeExtendedPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

#define ShapeTypeExtendedPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
#define ShapeTypeExtendedPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

#define ShapeTypeExtendedPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define ShapeTypeExtendedPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern ShapeTypeExtended*
ShapeTypeExtendedPluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern ShapeTypeExtended*
ShapeTypeExtendedPluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern ShapeTypeExtended*
ShapeTypeExtendedPluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPluginSupport_copy_data(
    ShapeTypeExtended *out,
    const ShapeTypeExtended *in);

NDDSUSERDllExport extern void 
ShapeTypeExtendedPluginSupport_destroy_data_w_params(
    ShapeTypeExtended *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
ShapeTypeExtendedPluginSupport_destroy_data_ex(
    ShapeTypeExtended *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
ShapeTypeExtendedPluginSupport_destroy_data(
    ShapeTypeExtended *sample);

NDDSUSERDllExport extern void 
ShapeTypeExtendedPluginSupport_print_data(
    const ShapeTypeExtended *sample,
    const char *desc,
    unsigned int indent);

NDDSUSERDllExport extern ShapeTypeExtended*
ShapeTypeExtendedPluginSupport_create_key_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern ShapeTypeExtended*
ShapeTypeExtendedPluginSupport_create_key(void);

NDDSUSERDllExport extern void 
ShapeTypeExtendedPluginSupport_destroy_key_ex(
    ShapeTypeExtendedKeyHolder *key,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
ShapeTypeExtendedPluginSupport_destroy_key(
    ShapeTypeExtendedKeyHolder *key);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
ShapeTypeExtendedPlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
ShapeTypeExtendedPlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
ShapeTypeExtendedPlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
ShapeTypeExtendedPlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
ShapeTypeExtendedPlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtended *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtended *out,
    const ShapeTypeExtended *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const ShapeTypeExtended *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtended *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
ShapeTypeExtendedPlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const ShapeTypeExtended *sample); 

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtended **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
ShapeTypeExtendedPlugin_deserialize_from_cdr_buffer(
    ShapeTypeExtended *sample,
    const char * buffer,
    unsigned int length);    

NDDSUSERDllExport extern RTIBool
ShapeTypeExtendedPlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
ShapeTypeExtendedPlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
ShapeTypeExtendedPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
ShapeTypeExtendedPlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
ShapeTypeExtendedPlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const ShapeTypeExtended * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
ShapeTypeExtendedPlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
ShapeTypeExtendedPlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
ShapeTypeExtendedPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const ShapeTypeExtended *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtended * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtended ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
ShapeTypeExtendedPlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtended *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_instance_to_key(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtendedKeyHolder *key, 
    const ShapeTypeExtended *instance);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_key_to_instance(
    PRESTypePluginEndpointData endpoint_data,
    ShapeTypeExtended *instance, 
    const ShapeTypeExtendedKeyHolder *key);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_instance_to_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    DDS_KeyHash_t *keyhash,
    const ShapeTypeExtended *instance);

NDDSUSERDllExport extern RTIBool 
ShapeTypeExtendedPlugin_serialized_sample_to_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    DDS_KeyHash_t *keyhash,
    RTIBool deserialize_encapsulation,
    void *endpoint_plugin_qos); 

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
ShapeTypeExtendedPlugin_new(void);

NDDSUSERDllExport extern void
ShapeTypeExtendedPlugin_delete(struct PRESTypePlugin *);

namespace ShapeServ {

    #define size_tPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define size_tPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define size_tPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define size_tPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define size_tPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern size_t*
    size_tPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern size_t*
    size_tPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern size_t*
    size_tPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    size_tPluginSupport_copy_data(
        size_t *out,
        const size_t *in);

    NDDSUSERDllExport extern void 
    size_tPluginSupport_destroy_data_w_params(
        size_t *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    size_tPluginSupport_destroy_data_ex(
        size_t *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    size_tPluginSupport_destroy_data(
        size_t *sample);

    NDDSUSERDllExport extern void 
    size_tPluginSupport_print_data(
        const size_t *sample,
        const char *desc,
        unsigned int indent);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    size_tPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        size_t *out,
        const size_t *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    size_tPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const size_t *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    size_tPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        size_t *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    size_tPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    size_tPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    size_tPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    size_tPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    size_tPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const size_t * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    size_tPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    size_tPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    size_tPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    size_tPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const size_t *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    size_tPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        size_t * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    size_tPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        size_t *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    cmd_tPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const cmd_t *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    cmd_tPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        cmd_t *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    cmd_tPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    cmd_tPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    cmd_tPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    cmd_tPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    cmd_tPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const cmd_t * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern unsigned int 
    cmd_tPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    cmd_tPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    cmd_tPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const cmd_t *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    cmd_tPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        cmd_t * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    cmd_tPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        cmd_t *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* ----------------------------------------------------------------------------
    Support functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern void
    cmd_tPluginSupport_print_data(
        const cmd_t *sample, const char *desc, int indent_level);

    #define cmd_typePlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define cmd_typePlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define cmd_typePlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define cmd_typePlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define cmd_typePlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern cmd_type*
    cmd_typePluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern cmd_type*
    cmd_typePluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern cmd_type*
    cmd_typePluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    cmd_typePluginSupport_copy_data(
        cmd_type *out,
        const cmd_type *in);

    NDDSUSERDllExport extern void 
    cmd_typePluginSupport_destroy_data_w_params(
        cmd_type *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    cmd_typePluginSupport_destroy_data_ex(
        cmd_type *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    cmd_typePluginSupport_destroy_data(
        cmd_type *sample);

    NDDSUSERDllExport extern void 
    cmd_typePluginSupport_print_data(
        const cmd_type *sample,
        const char *desc,
        unsigned int indent);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    cmd_typePlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    cmd_typePlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    cmd_typePlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    cmd_typePlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    cmd_typePlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        cmd_type *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    cmd_typePlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        cmd_type *out,
        const cmd_type *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    cmd_typePlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const cmd_type *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    cmd_typePlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        cmd_type *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    cmd_typePlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const cmd_type *sample); 

    NDDSUSERDllExport extern RTIBool 
    cmd_typePlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        cmd_type **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    cmd_typePlugin_deserialize_from_cdr_buffer(
        cmd_type *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    cmd_typePlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    cmd_typePlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    cmd_typePlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    cmd_typePlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    cmd_typePlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const cmd_type * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    cmd_typePlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    cmd_typePlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    cmd_typePlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    cmd_typePlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const cmd_type *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    cmd_typePlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        cmd_type * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    cmd_typePlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        cmd_type ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    cmd_typePlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        cmd_type *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    cmd_typePlugin_new(void);

    NDDSUSERDllExport extern void
    cmd_typePlugin_delete(struct PRESTypePlugin *);

    /* The type used to store keys for instances of type struct
    * AnotherSimple.
    *
    * By default, this type is struct shape_change_cmd
    * itself. However, if for some reason this choice is not practical for your
    * system (e.g. if sizeof(struct shape_change_cmd)
    * is very large), you may redefine this typedef in terms of another type of
    * your choosing. HOWEVER, if you define the KeyHolder type to be something
    * other than struct AnotherSimple, the
    * following restriction applies: the key of struct
    * shape_change_cmd must consist of a
    * single field of your redefined KeyHolder type and that field must be the
    * first field in struct shape_change_cmd.
    */
    typedef  class shape_change_cmd shape_change_cmdKeyHolder;

    #define shape_change_cmdPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define shape_change_cmdPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define shape_change_cmdPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define shape_change_cmdPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
    #define shape_change_cmdPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

    #define shape_change_cmdPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define shape_change_cmdPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern shape_change_cmd*
    shape_change_cmdPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern shape_change_cmd*
    shape_change_cmdPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern shape_change_cmd*
    shape_change_cmdPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPluginSupport_copy_data(
        shape_change_cmd *out,
        const shape_change_cmd *in);

    NDDSUSERDllExport extern void 
    shape_change_cmdPluginSupport_destroy_data_w_params(
        shape_change_cmd *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    shape_change_cmdPluginSupport_destroy_data_ex(
        shape_change_cmd *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    shape_change_cmdPluginSupport_destroy_data(
        shape_change_cmd *sample);

    NDDSUSERDllExport extern void 
    shape_change_cmdPluginSupport_print_data(
        const shape_change_cmd *sample,
        const char *desc,
        unsigned int indent);

    NDDSUSERDllExport extern shape_change_cmd*
    shape_change_cmdPluginSupport_create_key_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern shape_change_cmd*
    shape_change_cmdPluginSupport_create_key(void);

    NDDSUSERDllExport extern void 
    shape_change_cmdPluginSupport_destroy_key_ex(
        shape_change_cmdKeyHolder *key,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    shape_change_cmdPluginSupport_destroy_key(
        shape_change_cmdKeyHolder *key);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    shape_change_cmdPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    shape_change_cmdPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    shape_change_cmdPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    shape_change_cmdPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    shape_change_cmdPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd *out,
        const shape_change_cmd *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const shape_change_cmd *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmdPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const shape_change_cmd *sample); 

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmdPlugin_deserialize_from_cdr_buffer(
        shape_change_cmd *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    shape_change_cmdPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmdPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmdPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmdPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    shape_change_cmdPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const shape_change_cmd * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    shape_change_cmdPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmdPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmdPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const shape_change_cmd *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmdPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_instance_to_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmdKeyHolder *key, 
        const shape_change_cmd *instance);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_key_to_instance(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd *instance, 
        const shape_change_cmdKeyHolder *key);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_instance_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        DDS_KeyHash_t *keyhash,
        const shape_change_cmd *instance);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmdPlugin_serialized_sample_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        DDS_KeyHash_t *keyhash,
        RTIBool deserialize_encapsulation,
        void *endpoint_plugin_qos); 

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    shape_change_cmdPlugin_new(void);

    NDDSUSERDllExport extern void
    shape_change_cmdPlugin_delete(struct PRESTypePlugin *);

    /* The type used to store keys for instances of type struct
    * AnotherSimple.
    *
    * By default, this type is struct shape_change_cmd_wRequestor
    * itself. However, if for some reason this choice is not practical for your
    * system (e.g. if sizeof(struct shape_change_cmd_wRequestor)
    * is very large), you may redefine this typedef in terms of another type of
    * your choosing. HOWEVER, if you define the KeyHolder type to be something
    * other than struct AnotherSimple, the
    * following restriction applies: the key of struct
    * shape_change_cmd_wRequestor must consist of a
    * single field of your redefined KeyHolder type and that field must be the
    * first field in struct shape_change_cmd_wRequestor.
    */
    typedef  class shape_change_cmd_wRequestor shape_change_cmd_wRequestorKeyHolder;

    #define shape_change_cmd_wRequestorPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define shape_change_cmd_wRequestorPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define shape_change_cmd_wRequestorPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define shape_change_cmd_wRequestorPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
    #define shape_change_cmd_wRequestorPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

    #define shape_change_cmd_wRequestorPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define shape_change_cmd_wRequestorPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern shape_change_cmd_wRequestor*
    shape_change_cmd_wRequestorPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern shape_change_cmd_wRequestor*
    shape_change_cmd_wRequestorPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern shape_change_cmd_wRequestor*
    shape_change_cmd_wRequestorPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPluginSupport_copy_data(
        shape_change_cmd_wRequestor *out,
        const shape_change_cmd_wRequestor *in);

    NDDSUSERDllExport extern void 
    shape_change_cmd_wRequestorPluginSupport_destroy_data_w_params(
        shape_change_cmd_wRequestor *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    shape_change_cmd_wRequestorPluginSupport_destroy_data_ex(
        shape_change_cmd_wRequestor *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    shape_change_cmd_wRequestorPluginSupport_destroy_data(
        shape_change_cmd_wRequestor *sample);

    NDDSUSERDllExport extern void 
    shape_change_cmd_wRequestorPluginSupport_print_data(
        const shape_change_cmd_wRequestor *sample,
        const char *desc,
        unsigned int indent);

    NDDSUSERDllExport extern shape_change_cmd_wRequestor*
    shape_change_cmd_wRequestorPluginSupport_create_key_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern shape_change_cmd_wRequestor*
    shape_change_cmd_wRequestorPluginSupport_create_key(void);

    NDDSUSERDllExport extern void 
    shape_change_cmd_wRequestorPluginSupport_destroy_key_ex(
        shape_change_cmd_wRequestorKeyHolder *key,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    shape_change_cmd_wRequestorPluginSupport_destroy_key(
        shape_change_cmd_wRequestorKeyHolder *key);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    shape_change_cmd_wRequestorPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    shape_change_cmd_wRequestorPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    shape_change_cmd_wRequestorPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    shape_change_cmd_wRequestorPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    shape_change_cmd_wRequestorPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestor *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestor *out,
        const shape_change_cmd_wRequestor *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const shape_change_cmd_wRequestor *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestor *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmd_wRequestorPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const shape_change_cmd_wRequestor *sample); 

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestor **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmd_wRequestorPlugin_deserialize_from_cdr_buffer(
        shape_change_cmd_wRequestor *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    shape_change_cmd_wRequestorPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_wRequestorPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_wRequestorPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_wRequestorPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    shape_change_cmd_wRequestorPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const shape_change_cmd_wRequestor * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    shape_change_cmd_wRequestorPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_wRequestorPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_wRequestorPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const shape_change_cmd_wRequestor *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestor * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestor ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmd_wRequestorPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestor *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_instance_to_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestorKeyHolder *key, 
        const shape_change_cmd_wRequestor *instance);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_key_to_instance(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_wRequestor *instance, 
        const shape_change_cmd_wRequestorKeyHolder *key);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_instance_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        DDS_KeyHash_t *keyhash,
        const shape_change_cmd_wRequestor *instance);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_wRequestorPlugin_serialized_sample_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        DDS_KeyHash_t *keyhash,
        RTIBool deserialize_encapsulation,
        void *endpoint_plugin_qos); 

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    shape_change_cmd_wRequestorPlugin_new(void);

    NDDSUSERDllExport extern void
    shape_change_cmd_wRequestorPlugin_delete(struct PRESTypePlugin *);

    /* The type used to store keys for instances of type struct
    * AnotherSimple.
    *
    * By default, this type is struct shape_change_cmd_response
    * itself. However, if for some reason this choice is not practical for your
    * system (e.g. if sizeof(struct shape_change_cmd_response)
    * is very large), you may redefine this typedef in terms of another type of
    * your choosing. HOWEVER, if you define the KeyHolder type to be something
    * other than struct AnotherSimple, the
    * following restriction applies: the key of struct
    * shape_change_cmd_response must consist of a
    * single field of your redefined KeyHolder type and that field must be the
    * first field in struct shape_change_cmd_response.
    */
    typedef  class shape_change_cmd_response shape_change_cmd_responseKeyHolder;

    #define shape_change_cmd_responsePlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define shape_change_cmd_responsePlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define shape_change_cmd_responsePlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define shape_change_cmd_responsePlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
    #define shape_change_cmd_responsePlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

    #define shape_change_cmd_responsePlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define shape_change_cmd_responsePlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern shape_change_cmd_response*
    shape_change_cmd_responsePluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern shape_change_cmd_response*
    shape_change_cmd_responsePluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern shape_change_cmd_response*
    shape_change_cmd_responsePluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePluginSupport_copy_data(
        shape_change_cmd_response *out,
        const shape_change_cmd_response *in);

    NDDSUSERDllExport extern void 
    shape_change_cmd_responsePluginSupport_destroy_data_w_params(
        shape_change_cmd_response *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    shape_change_cmd_responsePluginSupport_destroy_data_ex(
        shape_change_cmd_response *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    shape_change_cmd_responsePluginSupport_destroy_data(
        shape_change_cmd_response *sample);

    NDDSUSERDllExport extern void 
    shape_change_cmd_responsePluginSupport_print_data(
        const shape_change_cmd_response *sample,
        const char *desc,
        unsigned int indent);

    NDDSUSERDllExport extern shape_change_cmd_response*
    shape_change_cmd_responsePluginSupport_create_key_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern shape_change_cmd_response*
    shape_change_cmd_responsePluginSupport_create_key(void);

    NDDSUSERDllExport extern void 
    shape_change_cmd_responsePluginSupport_destroy_key_ex(
        shape_change_cmd_responseKeyHolder *key,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    shape_change_cmd_responsePluginSupport_destroy_key(
        shape_change_cmd_responseKeyHolder *key);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    shape_change_cmd_responsePlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    shape_change_cmd_responsePlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    shape_change_cmd_responsePlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    shape_change_cmd_responsePlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    shape_change_cmd_responsePlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_response *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_response *out,
        const shape_change_cmd_response *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const shape_change_cmd_response *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_response *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmd_responsePlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const shape_change_cmd_response *sample); 

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_response **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmd_responsePlugin_deserialize_from_cdr_buffer(
        shape_change_cmd_response *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    shape_change_cmd_responsePlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_responsePlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_responsePlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_responsePlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    shape_change_cmd_responsePlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const shape_change_cmd_response * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    shape_change_cmd_responsePlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_responsePlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    shape_change_cmd_responsePlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const shape_change_cmd_response *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_response * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_response ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    shape_change_cmd_responsePlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_response *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_instance_to_key(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_responseKeyHolder *key, 
        const shape_change_cmd_response *instance);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_key_to_instance(
        PRESTypePluginEndpointData endpoint_data,
        shape_change_cmd_response *instance, 
        const shape_change_cmd_responseKeyHolder *key);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_instance_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        DDS_KeyHash_t *keyhash,
        const shape_change_cmd_response *instance);

    NDDSUSERDllExport extern RTIBool 
    shape_change_cmd_responsePlugin_serialized_sample_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        DDS_KeyHash_t *keyhash,
        RTIBool deserialize_encapsulation,
        void *endpoint_plugin_qos); 

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    shape_change_cmd_responsePlugin_new(void);

    NDDSUSERDllExport extern void
    shape_change_cmd_responsePlugin_delete(struct PRESTypePlugin *);

} /* namespace ShapeServ  */

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* ShapeTypePlugin_434675311_h */


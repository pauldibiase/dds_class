

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from HelloMsg.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef HelloMsg_1106059923_h
#define HelloMsg_1106059923_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

namespace Pax {
    static const DDS_Long MAX_STRING_SIZE= 128;
    static const DDS_Long MAX_SEQ_SIZE= 20;

    extern const char *HelloMsgTYPENAME;

    struct HelloMsgSeq;
    #ifndef NDDS_STANDALONE_TYPE
    class HelloMsgTypeSupport;
    class HelloMsgDataWriter;
    class HelloMsgDataReader;
    #endif

    class HelloMsg 
    {
      public:
        typedef struct HelloMsgSeq Seq;
        #ifndef NDDS_STANDALONE_TYPE
        typedef HelloMsgTypeSupport TypeSupport;
        typedef HelloMsgDataWriter DataWriter;
        typedef HelloMsgDataReader DataReader;
        #endif

        DDS_Char *   myText ;
        DDS_Long   id ;
        DDS_LongSeq  mySequence ;

    };
    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, start exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport __declspec(dllexport)
    #endif

    NDDSUSERDllExport DDS_TypeCode* HelloMsg_get_typecode(void); /* Type code */

    DDS_SEQUENCE(HelloMsgSeq, HelloMsg);                                        

    NDDSUSERDllExport
    RTIBool HelloMsg_initialize(
        HelloMsg* self);

    NDDSUSERDllExport
    RTIBool HelloMsg_initialize_ex(
        HelloMsg* self,RTIBool allocatePointers,RTIBool allocateMemory);

    NDDSUSERDllExport
    RTIBool HelloMsg_initialize_w_params(
        HelloMsg* self,
        const struct DDS_TypeAllocationParams_t * allocParams);        

    NDDSUSERDllExport
    void HelloMsg_finalize(
        HelloMsg* self);

    NDDSUSERDllExport
    void HelloMsg_finalize_ex(
        HelloMsg* self,RTIBool deletePointers);

    NDDSUSERDllExport
    void HelloMsg_finalize_w_params(
        HelloMsg* self,
        const struct DDS_TypeDeallocationParams_t * deallocParams);

    NDDSUSERDllExport
    void HelloMsg_finalize_optional_members(
        HelloMsg* self, RTIBool deletePointers);  

    NDDSUSERDllExport
    RTIBool HelloMsg_copy(
        HelloMsg* dst,
        const HelloMsg* src);

    #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
    /* If the code is building on Windows, stop exporting symbols.
    */
    #undef NDDSUSERDllExport
    #define NDDSUSERDllExport
    #endif
} /* namespace Pax  */

#endif /* HelloMsg */




/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from HelloMsg.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

#ifndef cdr_type_h
#include "cdr/cdr_type.h"
#endif    

#ifndef osapi_heap_h
#include "osapi/osapi_heap.h" 
#endif
#else
#include "ndds_standalone_type.h"
#endif

#include "HelloMsg.h"

namespace Pax {

    /* ========================================================================= */
    const char *HelloMsgTYPENAME = "Pax::HelloMsg";

    DDS_TypeCode* HelloMsg_get_typecode()
    {
        static RTIBool is_initialized = RTI_FALSE;

        static DDS_TypeCode HelloMsg_g_tc_myText_string = DDS_INITIALIZE_STRING_TYPECODE(((Pax::MAX_STRING_SIZE)));
        static DDS_TypeCode HelloMsg_g_tc_mySequence_sequence = DDS_INITIALIZE_SEQUENCE_TYPECODE(((Pax::MAX_SEQ_SIZE)),NULL);
        static DDS_TypeCode_Member HelloMsg_g_tc_members[3]=
        {

            {
                (char *)"myText",/* Member name */
                {
                    0,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"id",/* Member name */
                {
                    1,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_KEY_MEMBER , /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }, 
            {
                (char *)"mySequence",/* Member name */
                {
                    2,/* Representation ID */          
                    DDS_BOOLEAN_FALSE,/* Is a pointer? */
                    -1, /* Bitfield bits */
                    NULL/* Member type code is assigned later */
                },
                0, /* Ignored */
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                DDS_PUBLIC_MEMBER,/* Member visibility */
                1,
                NULL/* Ignored */
            }
        };

        static DDS_TypeCode HelloMsg_g_tc =
        {{
                DDS_TK_STRUCT,/* Kind */
                DDS_BOOLEAN_FALSE, /* Ignored */
                -1, /*Ignored*/
                (char *)"Pax::HelloMsg", /* Name */
                NULL, /* Ignored */      
                0, /* Ignored */
                0, /* Ignored */
                NULL, /* Ignored */
                3, /* Number of members */
                HelloMsg_g_tc_members, /* Members */
                DDS_VM_NONE  /* Ignored */         
            }}; /* Type code for HelloMsg*/

        if (is_initialized) {
            return &HelloMsg_g_tc;
        }

        HelloMsg_g_tc_mySequence_sequence._data._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

        HelloMsg_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&HelloMsg_g_tc_myText_string;

        HelloMsg_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

        HelloMsg_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)& HelloMsg_g_tc_mySequence_sequence;

        is_initialized = RTI_TRUE;

        return &HelloMsg_g_tc;
    }

    RTIBool HelloMsg_initialize(
        HelloMsg* sample) {
        return Pax::HelloMsg_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
    }

    RTIBool HelloMsg_initialize_ex(
        HelloMsg* sample,RTIBool allocatePointers, RTIBool allocateMemory)
    {

        struct DDS_TypeAllocationParams_t allocParams =
        DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

        allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
        allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

        return Pax::HelloMsg_initialize_w_params(
            sample,&allocParams);

    }

    RTIBool HelloMsg_initialize_w_params(
        HelloMsg* sample, const struct DDS_TypeAllocationParams_t * allocParams)
    {

        void* buffer = NULL;
        if (buffer) {} /* To avoid warnings */

        if (allocParams) {} /* To avoid warnings */

        if (allocParams->allocate_memory){
            sample->myText= DDS_String_alloc (((Pax::MAX_STRING_SIZE)));
            if (sample->myText == NULL) {
                return RTI_FALSE;
            }

        } else {
            if (sample->myText!= NULL) { 
                sample->myText[0] = '\0';
            }
        }

        if (!RTICdrType_initLong(&sample->id)) {
            return RTI_FALSE;
        }     

        if (allocParams->allocate_memory) {
            DDS_LongSeq_initialize(&sample->mySequence  );
            if (!DDS_LongSeq_set_maximum(&sample->mySequence , ((Pax::MAX_SEQ_SIZE)))) {
                return RTI_FALSE;
            }
        } else { 
            DDS_LongSeq_set_length(&sample->mySequence, 0);
        }
        return RTI_TRUE;
    }

    void HelloMsg_finalize(
        HelloMsg* sample)
    {

        Pax::HelloMsg_finalize_ex(sample,RTI_TRUE);
    }

    void HelloMsg_finalize_ex(
        HelloMsg* sample,RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParams =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

        if (sample==NULL) {
            return;
        } 

        deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

        Pax::HelloMsg_finalize_w_params(
            sample,&deallocParams);
    }

    void HelloMsg_finalize_w_params(
        HelloMsg* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
    {

        if (sample==NULL) {
            return;
        }
        if (deallocParams) {} /* To avoid warnings */

        if (sample->myText != NULL) {
            DDS_String_free(sample->myText);
            sample->myText=NULL;

        }

        DDS_LongSeq_finalize(&sample->mySequence);

    }

    void HelloMsg_finalize_optional_members(
        HelloMsg* sample, RTIBool deletePointers)
    {
        struct DDS_TypeDeallocationParams_t deallocParamsTmp =
        DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
        struct DDS_TypeDeallocationParams_t * deallocParams =
        &deallocParamsTmp;

        if (sample==NULL) {
            return;
        } 
        if (deallocParams) {} /* To avoid warnings */

        deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
        deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

    }

    RTIBool HelloMsg_copy(
        HelloMsg* dst,
        const HelloMsg* src)
    {

        if (!RTICdrType_copyStringEx (
            &dst->myText, src->myText, 
            ((Pax::MAX_STRING_SIZE)) + 1, RTI_FALSE)){
            return RTI_FALSE;
        }
        if (!RTICdrType_copyLong (
            &dst->id, &src->id)) { 
            return RTI_FALSE;
        }
        if (!DDS_LongSeq_copy(&dst->mySequence ,
        &src->mySequence )) {
            return RTI_FALSE;
        }

        return RTI_TRUE;
    }

    /**
    * <<IMPLEMENTATION>>
    *
    * Defines:  TSeq, T
    *
    * Configure and implement 'HelloMsg' sequence class.
    */
    #define T HelloMsg
    #define TSeq HelloMsgSeq
    #define T_initialize_w_params Pax::HelloMsg_initialize_w_params
    #define T_finalize_w_params   Pax::HelloMsg_finalize_w_params
    #define T_copy       Pax::HelloMsg_copy

    #ifndef NDDS_STANDALONE_TYPE
    #include "dds_c/generic/dds_c_sequence_TSeq.gen"
    #include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
    #else
    #include "dds_c_sequence_TSeq.gen"
    #include "dds_cpp_sequence_TSeq.gen"
    #endif

    #undef T_copy
    #undef T_finalize_w_params
    #undef T_initialize_w_params
    #undef TSeq
    #undef T
} /* namespace Pax  */




/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from HelloMsg.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef HelloMsgPlugin_1106059923_h
#define HelloMsgPlugin_1106059923_h

#include "HelloMsg.h"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

namespace Pax {

    /* The type used to store keys for instances of type struct
    * AnotherSimple.
    *
    * By default, this type is struct HelloMsg
    * itself. However, if for some reason this choice is not practical for your
    * system (e.g. if sizeof(struct HelloMsg)
    * is very large), you may redefine this typedef in terms of another type of
    * your choosing. HOWEVER, if you define the KeyHolder type to be something
    * other than struct AnotherSimple, the
    * following restriction applies: the key of struct
    * HelloMsg must consist of a
    * single field of your redefined KeyHolder type and that field must be the
    * first field in struct HelloMsg.
    */
    typedef  class HelloMsg HelloMsgKeyHolder;

    #define HelloMsgPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define HelloMsgPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define HelloMsgPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define HelloMsgPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
    #define HelloMsgPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

    #define HelloMsgPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define HelloMsgPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern HelloMsg*
    HelloMsgPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern HelloMsg*
    HelloMsgPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern HelloMsg*
    HelloMsgPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPluginSupport_copy_data(
        HelloMsg *out,
        const HelloMsg *in);

    NDDSUSERDllExport extern void 
    HelloMsgPluginSupport_destroy_data_w_params(
        HelloMsg *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    HelloMsgPluginSupport_destroy_data_ex(
        HelloMsg *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    HelloMsgPluginSupport_destroy_data(
        HelloMsg *sample);

    NDDSUSERDllExport extern void 
    HelloMsgPluginSupport_print_data(
        const HelloMsg *sample,
        const char *desc,
        unsigned int indent);

    NDDSUSERDllExport extern HelloMsg*
    HelloMsgPluginSupport_create_key_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern HelloMsg*
    HelloMsgPluginSupport_create_key(void);

    NDDSUSERDllExport extern void 
    HelloMsgPluginSupport_destroy_key_ex(
        HelloMsgKeyHolder *key,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    HelloMsgPluginSupport_destroy_key(
        HelloMsgKeyHolder *key);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    HelloMsgPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    HelloMsgPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    HelloMsgPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    HelloMsgPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    HelloMsgPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsg *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsg *out,
        const HelloMsg *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const HelloMsg *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsg *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    HelloMsgPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const HelloMsg *sample); 

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsg **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    HelloMsgPlugin_deserialize_from_cdr_buffer(
        HelloMsg *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    HelloMsgPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    HelloMsgPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    HelloMsgPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    HelloMsgPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    HelloMsgPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const HelloMsg * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    HelloMsgPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    HelloMsgPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    HelloMsgPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const HelloMsg *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsg * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsg ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    HelloMsgPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsg *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_instance_to_key(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsgKeyHolder *key, 
        const HelloMsg *instance);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_key_to_instance(
        PRESTypePluginEndpointData endpoint_data,
        HelloMsg *instance, 
        const HelloMsgKeyHolder *key);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_instance_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        DDS_KeyHash_t *keyhash,
        const HelloMsg *instance);

    NDDSUSERDllExport extern RTIBool 
    HelloMsgPlugin_serialized_sample_to_keyhash(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        DDS_KeyHash_t *keyhash,
        RTIBool deserialize_encapsulation,
        void *endpoint_plugin_qos); 

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    HelloMsgPlugin_new(void);

    NDDSUSERDllExport extern void
    HelloMsgPlugin_delete(struct PRESTypePlugin *);

} /* namespace Pax  */

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* HelloMsgPlugin_1106059923_h */

